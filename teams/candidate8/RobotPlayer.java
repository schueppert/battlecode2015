package candidate8;

import battlecode.common.*;
import static battlecode.common.RobotType.*;

public class RobotPlayer {
	public static void run(RobotController rc) throws GameActionException {

		if(rc.getType()==MISSILE){
			Missile.rc=rc;
			Missile.run();
			return;
		}
		play(standard(rc));
	}
	
	static Robot standard(RobotController rc) throws GameActionException{
		Robot r=new Robot(rc);
		RobotType type=rc.getType();
		switch(type){
		case HQ:
			Robot.stdBuild=new BuildOrder.LuxurySpawn(700);
			BuildOrder.bo=new RobotType[]{BEAVER,BEAVER};
			Robot.stdSupply=new Supply.HQSupply();
		break;
		case MINERFACTORY:
			Robot.stdBuild=BuildOrder.phaseOutSlow;
			BuildOrder.bo=new RobotType[]{MINER};
		break;
		case BARRACKS:
			Robot.stdBuild=new BuildOrder.LuxurySpawn(500);
			BuildOrder.bo=new RobotType[]{SOLDIER,SOLDIER,SOLDIER,SOLDIER,SOLDIER,SOLDIER,SOLDIER,SOLDIER,SOLDIER,SOLDIER};
		break;
		case AEROSPACELAB:
			Robot.stdBuild=BuildOrder.standardSpawn;
			BuildOrder.bo=new RobotType[]{LAUNCHER};
		break;
		case HELIPAD:
			Robot.stdBuild=new BuildOrder.LuxurySpawn(500);
			BuildOrder.bo=new RobotType[]{DRONE,DRONE,DRONE,DRONE,DRONE,DRONE,DRONE};
		break;
		case DRONE:
			int id=rc.readBroadcast(Robot.BC_SUPPLY_DRONE);
			try{
				if(Clock.getRoundNum()>300)
					rc.senseRobot(id);
				Robot.special=new Special[]{Special.oldDroneMovement,new Special.EndGameAttack(rc.getRoundLimit()-200)};
				
			}catch(Exception e){
				rc.broadcast(Robot.BC_SUPPLY_DRONE, rc.getID());
				Robot.stdSupply=new Supply.DroneSupply();
				Robot.stdMove=new Movement.Supply();
			}
			break;
		case BEAVER:
			Robot.stdBuild=new BuildOrder.LuxuryBuild(510);
			Movement.safety=TileCheck.limitToCloseHQ;
			if(Clock.getRoundNum()==0){
				BuildOrder.bo=new RobotType[]{MINERFACTORY,HELIPAD,HELIPAD,AEROSPACELAB};
			}
			else{
				BuildOrder.bo=new RobotType[]{AEROSPACELAB,HELIPAD,AEROSPACELAB};
			}
			Robot.stdMove=new Movement.Mine();
			break;
		case MINER:
			Robot.stdMove=new Movement.Mine();
			Movement.safety=TileCheck.isSafeAll;
			break;
		case LAUNCHER:
			Robot.stdMove=new Movement.Coward();
			Robot.special=new Special[]{Special.launchMissile,Special.followPathfinder};
			break;
		default:
			if(!type.isBuilding){
				Robot.stdMove=Movement.straight;
				Robot.special=new Special[]{Special.standAndFire,Special.followPathfinder,Special.chasing};
			}
		}
		return r;
	}
	
	static void play(Robot me){
		RobotController rc=Robot.rc;
		
		int errorCounter=0;
		while(true){
			try{
					int roundNum = Clock.getRoundNum();
					me.run();
					errorCounter=0;
					if(roundNum==Clock.getRoundNum())
						rc.yield();
				}
			catch(Exception e){
				errorCounter++;
				System.out.println("I threw an exception!");
				e.printStackTrace();
				if(errorCounter>2){
					errorCounter=0;
					rc.yield();
				}
			}
		}
	}
	
}
