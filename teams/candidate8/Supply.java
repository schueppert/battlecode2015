package candidate8;


import battlecode.common.*;
import static battlecode.common.RobotType.*;
import static battlecode.common.GameConstants.*;
import static candidate8.Robot.*;

public abstract class Supply {
	
	abstract boolean supply() throws GameActionException;
	
	
	static final class StandardSupply extends Supply{

		@Override
		boolean supply() throws GameActionException {
			double mySup=rc.getSupplyLevel();
			if(mySup<100)return false;
			double minOther=mySup;
			MapLocation target=null;
			RobotInfo friends[]=rc.senseNearbyRobots(SUPPLY_TRANSFER_RADIUS_SQUARED,myTeam);
			for(RobotInfo f:friends){
				if(f.type.needsSupply()&&f.supplyLevel<minOther){
					minOther=f.supplyLevel;
					target=f.location;
				}
			}
			if(mySup-minOther>100){
				rc.transferSupplies((int) ((mySup-minOther)/2), target);
				return true;
			}
			return false;
		}
		
		
	}

	static final class DroneSupply extends Supply{

		@Override
		boolean supply() throws GameActionException {
			double mySup=rc.getSupplyLevel();
			if(mySup<=500)return false;
			double minOther=200;
			int mult=0;
			MapLocation target=null;
			RobotInfo friends[]=rc.senseNearbyRobots(SUPPLY_TRANSFER_RADIUS_SQUARED,myTeam);
			for(RobotInfo f:friends){
				if(f.type.needsSupply()&&f.supplyLevel/f.type.supplyUpkeep<minOther){
					minOther=f.supplyLevel/f.type.supplyUpkeep;
					target=f.location;
					mult=f.type.supplyUpkeep;
				}
			}
			if(target!=null){
				int amount = (int) ((300-minOther)*mult);
				if(amount>mySup-500)amount=(int)(mySup-500);
				if(amount<0)return false;
				rc.transferSupplies(amount, target);
				return true;
			}
			return false;
		}
	}
	
	static final class HQSupply extends Supply{
		@Override
		boolean supply() throws GameActionException {
			int droneId = rc.readBroadcast(BC_SUPPLY_DRONE);
			double mySup=rc.getSupplyLevel();
			double minOther=300;
			int mult=0;
			int amount=0;
			MapLocation target=null;
			RobotInfo friends[]=rc.senseNearbyRobots(SUPPLY_TRANSFER_RADIUS_SQUARED,myTeam);
			for(RobotInfo f:friends){
				if(f.ID==droneId){
					if(f.supplyLevel<mySup){
						amount=(int)((mySup*5-f.supplyLevel)/6);
						rc.transferSupplies(amount, f.location);
						return true;
					}
				}
				if(f.type.needsSupply()&&f.supplyLevel/f.type.supplyUpkeep<minOther){
					minOther=f.supplyLevel/f.type.supplyUpkeep;
					target=f.location;
					mult=f.type.supplyUpkeep;
				}
			}
			if(target!=null){
				amount = (int) ((300-minOther)*mult);
				if(amount>mySup)amount=(int)(mySup);
				rc.transferSupplies(amount, target);
				return true;
			}
			return false;
		}
	}
	
	
}
