package candidate8;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import static candidate8.Pathfinder.*;
import static candidate8.Robot.*;

public class Headquarter {
	
	public static void run() throws GameActionException{
		if(Clock.getRoundNum()==0){
			init();
		}
	}
	
	public static void init() throws GameActionException{
		if(Robot.myHQ.x<Robot.enemyHQ.x){
			mnX=Robot.myHQ.x-originX;
			mxX=Robot.enemyHQ.x-originX;
		}else{
			mxX=Robot.myHQ.x-originX;
			mnX=Robot.enemyHQ.x-originX;
		}
		if(Robot.myHQ.y<Robot.enemyHQ.y){
			mnY=Robot.myHQ.y-originY;
			mxY=Robot.enemyHQ.y-originY;
		}else{
			mxY=Robot.myHQ.y-originY;
			mnY=Robot.enemyHQ.y-originY;
		}
		for(MapLocation t:myTowers){
			if(t.x-originX<mnX)mnX=t.x-originX;
			else if(t.x-originX>mxX)mxX=t.x-originX;
			if(t.y-originY<mnY)mnY=t.y-originY;
			else if(t.y-originY>mxY)mxY=t.y-originY;
		}
		for(MapLocation t:enemyTowers){
			if(t.x-originX<mnX)mnX=t.x-originX;
			else if(t.x-originX>mxX)mxX=t.x-originX;
			if(t.y-originY<mnY)mnY=t.y-originY;
			else if(t.y-originY>mxY)mxY=t.y-originY;
		}
		rc.broadcast(BC_MAP_X_MIN, mnX);
		rc.broadcast(BC_MAP_X_MAX, mxX);
		rc.broadcast(BC_MAP_Y_MIN, mnY);
		rc.broadcast(BC_MAP_Y_MAX, mxY);
	}
	
}
