package candidate8;

import battlecode.common.*;
import static battlecode.common.RobotType.*;
import static battlecode.common.GameConstants.*;
import static candidate8.Robot.*;

public abstract class TileCheck {
	abstract boolean check(MapLocation l);
	
	final static TileCheck isSafeAll = new TileCheck(){
		@Override
		boolean check(MapLocation l){
			for(RobotInfo enemy:enemies){
				if(enemy.type==RobotType.MISSILE&&enemy.location.distanceSquaredTo(l)<=8)return false;
				if(enemy.location.distanceSquaredTo(l)<=enemy.type.attackRadiusSquared)return false;
			}
			return isSafeTowers.check(l)&&isSafeHQ.check(l);
		}
	};
	
	final static TileCheck isSafeStructures=new TileCheck(){
		@Override
		boolean check(MapLocation l){
			return isSafeTowers.check(l)&&isSafeHQ.check(l);
		}
	};
	
	
	final static TileCheck isSafeTowers=new TileCheck(){
		@Override
		boolean check(MapLocation l){
			for(MapLocation tower:enemyTowers){
				if(l.distanceSquaredTo(tower)<=TOWER.attackRadiusSquared)return false;
			}
			return true;
		}
	};
	
	final static TileCheck isSafeHQ=new TileCheck(){
		@Override
		boolean check(MapLocation l){
			if(enemyTowers.length>=5){
				if(enemyHQ.distanceSquaredTo(l.add(l.directionTo(enemyHQ)))<HQ_BUFFED_ATTACK_RADIUS_SQUARED)return false;
			}
			else if(enemyTowers.length>=2){
				if(enemyHQ.distanceSquaredTo(l)<HQ_BUFFED_ATTACK_RADIUS_SQUARED)return false;
			}else if(enemyHQ.distanceSquaredTo(l)<HQ.attackRadiusSquared)return false;
			return true;
		}
	};
	
	final static TileCheck endGame =new TileCheck(){
		@Override
		boolean check(MapLocation l){
			if(enemyTowers.length>=5){
				if(enemyHQ.distanceSquaredTo(l.add(l.directionTo(enemyHQ)))<HQ_BUFFED_ATTACK_RADIUS_SQUARED)return false;
			}
			else if(enemyTowers.length>=3)
				if(enemyHQ.distanceSquaredTo(l)<HQ_BUFFED_ATTACK_RADIUS_SQUARED)return false;
			return true;
		}
	};
	
	final static TileCheck always=new TileCheck(){
		@Override
		boolean check(MapLocation l){
			return true;
		}
	};
	

	final static TileCheck limitToCloseHQ=new TileCheck(){
		@Override
		boolean check(MapLocation l){
			return l.distanceSquaredTo(myHQ)<=(2+Clock.getRoundNum()*Clock.getRoundNum()/20000*10)&&isSafeAll.check(l);
		}
	};
	
	int odd=1;
	
	final static TileCheck checker=new TileCheck(){
		{
			odd=(myHQ.x+myHQ.y)%2;
		}
		@Override
		boolean check(MapLocation l){
			return (l.x+l.y)%2==odd&&limitToCloseHQ.check(l);
		}
	};
}
