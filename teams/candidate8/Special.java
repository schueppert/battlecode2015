package candidate8;

import battlecode.common.*;
import static battlecode.common.RobotType.*;
import static candidate8.Robot.*;

public abstract class Special {
	
	abstract void run() throws GameActionException;
	
	static final Special pathToHQ = new Special(){
		@Override
		void run() throws GameActionException{
			facing=myLocation.directionTo(enemyHQ);
		}
	};
	
	static final Special attackWave=new Special(){
		@Override
		void run() throws GameActionException{
			if(Clock.getRoundNum()>1200){
				Movement.safety=TileCheck.always;
				facing=myLocation.directionTo(enemyHQ);
			}
		}
	};
	
	static final Special chasing=new Special(){
		@Override
		void run() throws GameActionException{
			if(enemies.length>0){
				facing=myLocation.directionTo(enemies[0].location);
			}
		}
	};
	
	static Movement remvdmv=null;
	
	static final Special standAndFire=new Special(){
		@Override
		void run() throws GameActionException{
			RobotInfo ens[]=rc.senseNearbyRobots(attackRange, enemyTeam);
			boolean stay=ens.length>0;
			if(attackRange>24&&!stay){
				for(MapLocation tower:enemyTowers){
					if(tower.distanceSquaredTo(myLocation)<=attackRange){
						stay=true;
						break;
					}
				}
			}
			if(stay&&Robot.stdMove!=null){
				remvdmv=Robot.stdMove;
				Robot.stdMove=null;
			}else if(!stay&&remvdmv!=null){
				Robot.stdMove=remvdmv;
				remvdmv=null;
			}
		}
	};
	
	boolean barrage=false;
	boolean fired=false;
	static final Special launchMissile=new Special(){
		@Override
		void run() throws GameActionException{
			if(fired){
				fired=false;
				return;
			}
			if(rc.getMissileCount()>0){
				RobotInfo ens[]=rc.senseNearbyRobots(attackRange, enemyTeam);
				for(RobotInfo enemy:ens){
					if(enemy.type==MISSILE)continue;
					if(rc.getMissileCount()==0)return;
					MapLocation target= enemy.location;
					Direction dir=myLocation.directionTo(target);
					if(rc.canLaunch(dir)){
						rc.launchMissile(dir);
						fired=true;
						return;
					}
					if(rc.canLaunch(dir.rotateRight())){
						rc.launchMissile(dir.rotateRight());
						fired=true;
						return;
					}
					if(rc.canLaunch(dir.rotateLeft())){
						rc.launchMissile(dir.rotateLeft());
						fired=true;
						return;
					}
				}
				if(rc.getMissileCount()==5||barrage){
					for(MapLocation tower:enemyTowers){
						if(myLocation.distanceSquaredTo(tower)<=attackRange){
							if(rc.getMissileCount()==0)return;
							MapLocation target= tower;
							Direction dir=myLocation.directionTo(target);
							if(rc.canLaunch(dir)){
								rc.launchMissile(dir);
								return;
							}
							if(rc.canLaunch(dir.rotateRight())){
								rc.launchMissile(dir.rotateRight());
								fired=true;
								return;
							}
							if(rc.canLaunch(dir.rotateLeft())){
								rc.launchMissile(dir.rotateLeft());
								fired=true;
								return;
							}
						}
					}
					if(myLocation.distanceSquaredTo(enemyHQ)<=attackRange){
						if(rc.getMissileCount()==0)return;
						MapLocation target= enemyHQ;
						Direction dir=myLocation.directionTo(target);
						if(rc.canLaunch(dir)){
							rc.launchMissile(dir);
							fired=true;
							return;
						}
						if(rc.canLaunch(dir.rotateRight())){
							rc.launchMissile(dir.rotateRight());
							fired=true;
							return;
						}
						if(rc.canLaunch(dir.rotateLeft())){
							rc.launchMissile(dir.rotateLeft());
							fired=true;
							return;
						}
					}
					if(rc.getMissileCount()==0)barrage=false;
				}
			}
		}
	};
	
	static final Special dumbSeekHq = new Special(){
		void run(){
			if(Clock.getRoundNum()%100==0)
				facing=myLocation.directionTo(enemyHQ);
		}
	};
	static final Special dumbSeekMyHq = new Special(){
		void run(){
			facing=myLocation.directionTo(myHQ);
		}
	};
	
	
	boolean kiting=false;
	static final Special oldDroneMovement = new Special(){
		void run(){
			boolean slow =rc.senseTerrainTile(myLocation)==TerrainTile.VOID;
			int distance=20;
			MapLocation target=rc.senseEnemyHQLocation();
			if(kiting){
				facing=facing.opposite();
				kiting=false;
			}
			else{
				int senserange=50;
				if(Clock.getRoundNum()-birthTime<30)senserange=950-30*(Clock.getRoundNum()-birthTime);
				distance=target.distanceSquaredTo(myLocation);
				facing=myLocation.directionTo(target);
				for(RobotInfo enemy:rc.senseNearbyRobots(senserange,enemyTeam)){
					if(slow&&enemy.type==LAUNCHER&&enemy.location.distanceSquaredTo(myLocation)<=60){
						facing=enemy.location.directionTo(myLocation);
						break;
					}
					if(slow&&enemy.type==COMMANDER&&enemy.location.distanceSquaredTo(myLocation)<=35){
						facing=enemy.location.directionTo(myLocation);
						break;
					}
					if(enemy.type==MISSILE&&(enemy.location.distanceSquaredTo(myLocation)<=(slow?60:15))){
						facing=enemy.location.directionTo(myLocation);
						break;
					}
					if(enemy.location.distanceSquaredTo(myLocation)<distance){
						target=enemy.location;
						distance=target.distanceSquaredTo(myLocation);
						facing=myLocation.directionTo(target);
					}
				}

				for(RobotInfo missile:rc.senseNearbyRobots(8, myTeam)){
					if(missile.type!=MISSILE)continue;
					if(missile.location.distanceSquaredTo(myLocation)<=8){
						facing=missile.location.directionTo(myLocation);
						break;
					};
				}
			}
			RobotInfo r[]=rc.senseNearbyRobots(attackRange,enemyTeam);
			if(r.length>0){
				int x=0;
				int y=0;
				for(RobotInfo ri:enemies){
					MapLocation temp=ri.location;
					x+=(temp.x-myLocation.x)*360/(temp.distanceSquaredTo(myLocation));
					y+=(temp.y-myLocation.y)*360/(temp.distanceSquaredTo(myLocation));
				}
				if(x!=0||y!=0)
					facing=new MapLocation(myLocation.x+x,myLocation.y+y).directionTo(myLocation);
				else facing=myLocation.directionTo(myHQ);
				kiting=true;
			}
			if(distance>attackRange||!TileCheck.isSafeAll.check(rc.getLocation())){
					if(rc.senseNearbyRobots(target,24,enemyTeam).length*3+enemies.length*3<rc.senseNearbyRobots(target,24,myTeam).length)
						stdMove=Movement.moveSafe;
					else
						stdMove=Movement.harrass;
			}else{
				stdMove=null;
			}
		}
	};
	
	static final class LimitTurns extends Special{
		final Special action;
		final int timer;
		LimitTurns(Special action, int timer){
			this.action=action;
			this.timer=timer;
		}
		void run() throws GameActionException{
			if(Clock.getRoundNum()<=timer)
				action.run();
		}
	}
	
	static final Special followPathfinder = new Special() {
		void run() throws GameActionException{
			if(Clock.getRoundNum()%2==0){
				int d=Pathfinder.getDist(myLocation)*2;
				Direction f=facing;
				facing=null;
				for(int i=0;i<8;i++){
					if(!rc.canMove(f))continue;
					int temp=Pathfinder.getDist(myLocation.add(f));
					temp=temp*2+temp%2;
					if(temp<d){
						d=temp;
						facing=f;
					}
					f=f.rotateRight();
				}
				if(facing==null)facing=myLocation.directionTo(enemyHQ);
			}
		}
	};
	
	
	static final class CountUnit extends Special{
		int channel;
		CountUnit(int channel){
			this.channel=channel;
		}
		@Override
		void run() throws GameActionException {
			rc.broadcast(channel, rc.readBroadcast(channel)+1);
		}
	};
	static final class ReadUnitCount extends Special{
		int readChannel,outputChannel;
		ReadUnitCount(int read,int out){
			readChannel=read;
			outputChannel=out;
		}
		@Override
		void run() throws GameActionException {
			rc.broadcast(outputChannel, rc.readBroadcast(readChannel));
			rc.broadcast(readChannel, 0);
			
		}
		
	}
	
	static final class EndGameAttack extends Special{
		final int round;
		EndGameAttack(int round){
			this.round=round;
		}
		@Override
		void run() throws GameActionException {
			if(Clock.getRoundNum()<round-20||myID%5==0||birthTime>round)return;
			if(Clock.getRoundNum()<round){
				facing=enemyHQ.directionTo(myLocation);
				return;
			}
			Robot.stdMove=Movement.straight;
			Movement.safety=TileCheck.endGame;
			MapLocation target=null;
			if (enemyTowers.length > 0) {
				float distance = myLocation.distanceSquaredTo(enemyTowers[0])*10000+enemyTowers[0].x+enemyTowers[0].y*147;
				target = enemyTowers[0];
				for (MapLocation t : enemyTowers) {
					float tempdist = myLocation.distanceSquaredTo(t)*10000+t.x+t.y*147;;
					if (tempdist < distance) {
						target = t;
						distance = tempdist;
					}
				}
			} else
				target = rc.senseEnemyHQLocation();
			facing = myLocation.directionTo(target);
			
		}
		
	}
}
