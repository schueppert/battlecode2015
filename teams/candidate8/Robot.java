package candidate8;

import java.util.Random;

import battlecode.common.*;
import static battlecode.common.RobotType.*;

public class Robot {
	static final int BC_SUPPLY_DRONE=10;
	static final int BC_SUPPLY_CALL=11;
	

	static final int BC_DRONE_COUNT=32;
	static final int BC_DRONE_CAST=33;
	
	static RobotController rc;
	static RobotInfo friends[], enemies[];
	static RobotType myType;
	static MapLocation myHQ, myLocation, enemyHQ, myTowers[], enemyTowers[];
	static Team myTeam, enemyTeam;
	static int attackRange, myID, birthTime;
	static boolean canBuild,canShoot, left,supplyDrone;
	static Direction facing, direction[];
	static Random rand;
	static float roll;
	

	static Movement stdMove=null;
	static BuildOrder stdBuild=null;
	static Supply stdSupply=new Supply.StandardSupply();
	static Special special[]={};

	Robot(RobotController rcc) throws GameActionException {
		rc = rcc;
		Robot.birthTime=Clock.getRoundNum();
		direction=Direction.values();
		myID=rc.getID();
		rand=new Random(myID);
		facing=direction[rand.nextInt(8)];
		myTeam = rcc.getTeam();
		myType=rcc.getType();
		attackRange=myType==LAUNCHER?63:myType.attackRadiusSquared;
		canBuild=myType.canBuild();
		enemyTeam = myTeam.opponent();
		enemyHQ=rc.senseEnemyHQLocation();
		myHQ=rc.senseHQLocation();
		left=rand.nextBoolean();
		roll=rand.nextFloat();
		Pathfinder.init();
		enemyTowers=rc.senseEnemyTowerLocations();
		myTowers=rc.senseEnemyTowerLocations();
		myLocation=rc.getLocation();
		if(myType==RobotType.HQ){
			Headquarter.run();
		}
	}

	void turnInitialization() {
		roll=rand.nextFloat();
		if(roll>0.98)left=!left;
		myLocation=rc.getLocation();
		enemies = rc.senseNearbyRobots(35,enemyTeam);
		friends = rc.senseNearbyRobots(24,myTeam);
		{
			int k=0;
			for(RobotInfo friend:friends){
				if(friend.type==MISSILE)k++;
			}
		}
		enemyTowers=rc.senseEnemyTowerLocations();
		Debug.debug_setLocation(myLocation);
		Debug.debug_coreAction("Waiting.");
	}

	void run() throws GameActionException {
		turnInitialization();
		stdSupply.supply();
		if(myType.canAttack()&&rc.getWeaponDelay()<1){
			shootSomething();
		}
		for(Special abil:special){
			abil.run();
		}
		if(rc.getCoreDelay()<1){
			if(stdBuild!=null)stdBuild.build();
		}
		if(rc.getCoreDelay()<1&&stdMove!=null&&stdMove.move())Debug.debug_setLocation(myLocation.add(facing));
		
		if (myType==HQ)
			Pathfinder.doBFS(9600);
		else
			Pathfinder.doBFS(1800);
		
		rc.setIndicatorString(2, "Distance from HQ: " + Pathfinder.getDist(rc.getLocation()));
	}
	
	boolean shootSomething() throws GameActionException{
		RobotInfo r[]=rc.senseNearbyRobots(attackRange,enemyTeam);
		MapLocation target=null;
		double hp=99999999;
		if(myType==DRONE){
			for(RobotInfo ri:r){
				double temp=ri.health;
				if(ri.type==TOWER)temp=8;
				if(ri.type==HQ)temp=-1;
				if(temp<hp&&ri.type!=MISSILE){
					hp=temp;target=ri.location;
				}
			}
		}else {
			for(RobotInfo ri:r){
				double temp=ri.health;
				if(ri.type==MISSILE)temp*=2000;
				if(ri.type==TOWER)temp=8;
				if(ri.type==HQ)temp=-1;
				if(temp<hp){
					hp=temp;target=ri.location;
				}
			}
		}
		if(target!=null)
		rc.attackLocation(target);
		return target!=null;
	}
	

}
