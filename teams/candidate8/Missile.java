package candidate8;

import battlecode.common.*;

public class Missile {
	static RobotController rc;
	
	public static void run(){
		int lifeTime=7;
		Direction facing;
		while(true){
			try{
				if(rc.isCoreReady()){
					missileMove(lifeTime);
				}
				{
					RobotInfo enemies[]=rc.senseNearbyRobots(2,rc.getTeam().opponent());
					int count=0;
					for(RobotInfo enemy:enemies){
						if(enemy.type!=RobotType.MISSILE){
							rc.explode();
							break;
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(!rc.isCoreReady()){
				rc.yield();
				lifeTime--;
			}
			else lifeTime--;
		}
	}
	
	static void missileMove(int lifeTime) throws GameActionException{
		int range=lifeTime*lifeTime-1;
		MapLocation myLocation = rc.getLocation();
		RobotInfo enemies[]=rc.senseNearbyRobots(range,rc.getTeam().opponent());
		if(enemies.length>0){
			move(rc.getLocation().directionTo(enemies[0].location));
			return;
		}
		for(MapLocation tower:rc.senseEnemyTowerLocations()){
			if(myLocation.distanceSquaredTo(tower)<range){
				move(myLocation.directionTo(tower));
				return;
			}
		}
		move(myLocation.directionTo(rc.senseEnemyHQLocation()));
	}
	static void move(Direction facing) throws GameActionException{
		if(rc.canMove(facing))rc.move(facing);
		else if(rc.canMove(facing.rotateLeft()))rc.move(facing.rotateLeft());
		else if(rc.canMove(facing.rotateRight()))rc.move(facing.rotateRight());
	}

}
