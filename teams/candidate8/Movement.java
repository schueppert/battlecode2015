package candidate8;


import battlecode.common.*;
import static battlecode.common.RobotType.*;
import static candidate8.Robot.*;

public abstract class Movement {
	
	abstract boolean move() throws GameActionException;
	
	static TileCheck safety=TileCheck.isSafeStructures;

	static final Movement straight=new Straight();
	
	static final Movement flee= new Movement(){
		boolean move() throws GameActionException{
			Direction beg=facing;
			int x=0;
			int y=0;
			for(RobotInfo ri:enemies){
				MapLocation temp=ri.location;
				x+=(temp.x-myLocation.x)*360/(temp.distanceSquaredTo(myLocation));
				y+=(temp.y-myLocation.y)*360/(temp.distanceSquaredTo(myLocation));
			}
			if(x!=0||y!=0)
				facing=new MapLocation(myLocation.x+x,myLocation.y+y).directionTo(myLocation);
			else facing=myLocation.directionTo(myHQ);
			boolean ret= straight.move();
			facing=beg;
			return ret;
		}
	};

	static final class Random extends Movement{
		@Override 
		boolean move() throws GameActionException{
			if(roll<0.05)facing=facing.rotateLeft();
			else if(roll<0.1)facing=facing.rotateRight();
			return straight.move();
		}
	};
	
	static final class Straight extends Movement{
		@Override 
		boolean move() throws GameActionException{
			int i=1,sign=left?-1:1,d=facing.ordinal()+8;
			Direction dir;
			for(;i<=8;d+=i*sign,i++,sign*=-1){
				dir=direction[d%8];
				if(!rc.canMove(dir)||!safety.check(myLocation.add(dir)))continue;
				rc.move(dir);
				facing=dir;
				Debug.debug_coreAction("moving.");
				return true;
			}
			return false;
		}
	};
	
	static final Movement circle = new Movement(){
		@Override 
		boolean move() throws GameActionException{
			outer:for(int p=0;p++<8;facing=left?facing.rotateLeft():facing.rotateRight()){
				if(!rc.canMove(facing)||!safety.check(myLocation.add(facing)))continue;
				rc.move(facing);
				return true;
			}
			return false;
		}
	};
	
	static final class Coward extends Movement{
		boolean move() throws GameActionException {
			if(enemies.length>0)return flee.move();
			else {
				if(rc.senseNearbyRobots(attackRange,enemyTeam).length>0)return false;
				for(MapLocation tower:enemyTowers){
					if(myLocation.distanceSquaredTo(tower)<=attackRange){
						return false;
					}
				}
				if(myLocation.distanceSquaredTo(enemyHQ)<=attackRange)return false;
			}
			return straight.move();
		}
	}
	

	static final Movement moveSafe = new  Movement(){
		boolean move() throws GameActionException {
			for(RobotInfo enemy:enemies){
				if(enemy.type==RobotType.MISSILE&&enemy.location.distanceSquaredTo(rc.getLocation())<10)facing=enemy.location.directionTo(rc.getLocation());
			}
			return straight.move();
		}
	};
	
	static int mineturns=0;
	
	static final class Mine extends Movement{
		
		@Override
		boolean move() throws GameActionException {
			if(!TileCheck.isSafeAll.check(myLocation))return flee.move();
			if(roll<0.05)facing=facing.rotateLeft();
			else if(roll<0.1)facing=facing.rotateRight();
			if(mineturns--==0){
				double maxOre=rc.senseOre(myLocation),curOre;
				int i=1,sign=left?-1:1,d=facing.ordinal()+8;
				Direction dir,maxdir=null;
				for(;i<=8;d+=i*sign,i++,sign*=-1){
					dir=direction[d%8];
					if((curOre=rc.senseOre(myLocation.add(dir)))<=maxOre||!rc.canMove(dir)||!safety.check(myLocation.add(dir)))continue;
					maxOre=curOre;
					maxdir=dir;
				}
				if(maxdir==null){
					if(straight.move())
						return true;
				}
				else{
					facing=maxdir;
					rc.move(facing);
					return true;
				}
			}
			if(mineturns<0){
				mineturns=(int) (rc.senseOre(myLocation)/4+0.99);
				if(mineturns>6)mineturns=6;
			}
			Debug.debug_coreAction("Mining, "+mineturns+" times left.");
			if(mineturns>0){
				rc.mine();
				return true;
			}
			return false;
		}
		
	};
	
	static final Movement harrass = new Movement(){
		boolean move() throws GameActionException {

			boolean miner=false;
			if(rand.nextFloat()>0.9){
				if(rand.nextFloat()>0.5)facing=facing.rotateLeft();
				else facing=facing.rotateRight();
			}
			outer:for(int p=0;p++<8;facing=left?facing.rotateLeft():facing.rotateRight()){
				if(!rc.canMove(facing))continue;
				MapLocation next=rc.getLocation().add(facing);
				for(RobotInfo enemy:enemies){
					if(enemy.type==MISSILE&&enemy.location.distanceSquaredTo(next)<=8)continue;
					else if((enemy.weaponDelay<=1||(enemy.type==RobotType.TANK&&enemy.weaponDelay<=2))&&enemy.location.distanceSquaredTo(next)<=enemy.type.attackRadiusSquared){
						if(enemy.type==RobotType.BEAVER)continue;
						if(enemy.type==RobotType.MINER){
							if(miner)continue outer;
							else miner=true;
						}else
						continue outer;
					}
				}
				if(!TileCheck.isSafeStructures.check(next))continue;
				rc.move(facing);
				return true;
			}
			facing=left?facing.rotateLeft():facing.rotateRight();
			if(!TileCheck.isSafeAll.check(myLocation))
				return circle.move();
			return false;
		}
	};
	
	static final class Supply extends Movement{
		static boolean fetching=false;
		@Override
		boolean move() throws GameActionException {
			if(fetching){
				facing=myLocation.directionTo(myHQ);
				fetching=rc.getSupplyLevel()<7500;
			}
			else if(enemies.length>0)facing=myLocation.directionTo(myHQ);
			else if(rc.getSupplyLevel()<1000){
				fetching=true;
				facing=myLocation.directionTo(myHQ);
			}else facing=myLocation.directionTo(enemyHQ);
			straight.move();
			return false;
		}
		
	}
	
}
