package candidate8;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;
import static candidate8.Robot.*;

public class Pathfinder {
	static int originX;
	static int originY;

	static int mnX;
	static int mxX;
	static int mnY;
	static int mxY;
	
	static void debug_borders(){
		rc.setIndicatorString(0,String.format("mnX:%d, mxX:%d, mnY:%d, mxY: %d", mnX,mxX,mnY,mxY));
	}

	static final int QUEUE_MAX_SIZE = 1000;

	static final int BC_MAP_X_MIN = 1000;
	static final int BC_MAP_X_MAX = BC_MAP_X_MIN + 1;
	static final int BC_MAP_Y_MIN = BC_MAP_X_MAX + 1;
	static final int BC_MAP_Y_MAX = BC_MAP_Y_MIN + 1;
	static final int BC_BFS_INITED = BC_MAP_Y_MAX + 1;
	static final int BC_VERSION = BC_BFS_INITED + 1;
	static final int BC_QUEUE_FIRST = BC_VERSION + 1;
	static final int BC_QUEUE_LAST = BC_QUEUE_FIRST + 1;
	static final int BC_QUEUE_BEGIN = BC_QUEUE_LAST + 1;
	static final int BC_QUEUE_END = BC_QUEUE_BEGIN + QUEUE_MAX_SIZE;
	static final int BC_FIELD_BASE = BC_QUEUE_END + 1;

	static void init() {
		originX = (Robot.enemyHQ.x + Robot.myHQ.x) / 2 - 128;
		originY = (Robot.enemyHQ.y + Robot.enemyHQ.y) / 2 - 128;
	}

	/**
	 * Plz set (xMax - xMin <= 120) && (yMax - yMin <= 120)
	 */
	static void setMapBorders(int xMin, int xMax, int yMin, int yMax) {
		try {
			rc.broadcast(BC_MAP_X_MIN, xMin - originX);
			rc.broadcast(BC_MAP_X_MAX, xMax - originX);
			rc.broadcast(BC_MAP_Y_MIN, yMin - originY);
			rc.broadcast(BC_MAP_Y_MAX, yMax - originY);
			rc.broadcast(BC_BFS_INITED, 0);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

	static int[] dx = { -1, -1, -1, 0, 0, 1, 1, 1 };
	static int[] dy = { -1, 0, 1, -1, 1, -1, 0, 1 };

	// 1 << 7 == 128
	static final int INDEX_SHIFT = 7;
	static final int INDEX_MASK = (1 << INDEX_SHIFT) - 1;

	static final int X_SHIFT = (1 << INDEX_SHIFT);
	static final int Y_SHIFT = 1;
	static int[] dxIndex = { -X_SHIFT, -X_SHIFT, -X_SHIFT, 0, 0, X_SHIFT,
			X_SHIFT, X_SHIFT };
	static int[] dyIndex = { -Y_SHIFT, 0, Y_SHIFT, -Y_SHIFT, Y_SHIFT, -Y_SHIFT,
			0, Y_SHIFT };
	static int[] dIndex = { -X_SHIFT - Y_SHIFT, -X_SHIFT - 0,
			-X_SHIFT + Y_SHIFT, 0 - Y_SHIFT, 0 + Y_SHIFT, X_SHIFT - Y_SHIFT,
			X_SHIFT + 0, X_SHIFT + Y_SHIFT };

	static int[] xy = new int[2];

	static void makeXY(int index) {
		index -= BC_FIELD_BASE;
		int xmap = index >> INDEX_SHIFT;
		int ymap = index & INDEX_MASK;
		while (xmap < mnX)
			xmap += 128;
		while (ymap < mnY)
			ymap += 128;
		xy[0] = xmap;
		xy[1] = ymap;
	}

	static int getIndex(int x, int y) {
		return ((x & 0x7F) << INDEX_SHIFT) + (y & 0x7F) + BC_FIELD_BASE;
	}

	static int getDist(MapLocation location) throws GameActionException {
		return (rc
				.readBroadcast((((location.x - originX) & 0x7F) << INDEX_SHIFT)
						+ ((location.y - originY) & 0x7F) + BC_FIELD_BASE)) & 0xFFFF;
	}
	
	static void updateBorders() throws GameActionException{
		mnX = rc.readBroadcast(BC_MAP_X_MIN);
		mxX = rc.readBroadcast(BC_MAP_X_MAX);
		mnY = rc.readBroadcast(BC_MAP_Y_MIN);
		mxY = rc.readBroadcast(BC_MAP_Y_MAX);
		int x = Robot.myLocation.x-originX;
		int y = Robot.myLocation.y-originY;
		if(x-mnX<=4){
			if(rc.senseTerrainTile(new MapLocation(x-4+originX,y+originY))!=TerrainTile.OFF_MAP){
				mnX=x-4;
				rc.broadcast(BC_MAP_X_MIN, mnX);
			}
		}
		if(mxX-x<=4){
			if(rc.senseTerrainTile(new MapLocation(x+4+originX,y+originY))!=TerrainTile.OFF_MAP){
				mxX=x+4;
				rc.broadcast(BC_MAP_X_MAX, mxX);
			}
		}
		if(y-mnY<=4){
			if(rc.senseTerrainTile(new MapLocation(x+originX,y-4+originY))!=TerrainTile.OFF_MAP){
				mnY=y-4;
				rc.broadcast(BC_MAP_Y_MIN, mnY);
			}
		}
		if(mxY-y<=4){
			if(rc.senseTerrainTile(new MapLocation(x+originX,y+4+originY))!=TerrainTile.OFF_MAP){
				mxY=y+4;
				rc.broadcast(BC_MAP_Y_MAX, mxY);
			}
		}
		
	}

	static void doBFS(int endTime) throws GameActionException {
		updateBorders();
		if (Clock.getBytecodeNum() < endTime - 666) {
			SharedQueue.load();
			int currentVersion = rc.readBroadcast(BC_VERSION);
			if (rc.readBroadcast(BC_BFS_INITED) == 0) {
				if (Clock.getBytecodeNum() < endTime - 1555) {
					// int start = Clock.getBytecodeNum();
					++currentVersion;
					rc.broadcast(BC_VERSION, currentVersion);

					SharedQueue.reset();

					int hqX = Robot.enemyHQ.x - originX;
					int hqY = Robot.enemyHQ.y - originY;
					int hqIndex = ((hqX & 0x7F) << INDEX_SHIFT) + (hqY & 0x7F)
							+ BC_FIELD_BASE;

					rc.broadcast(hqIndex, ((currentVersion + 1) << 16) + 0);

					for (int dir = 0; dir < 8; dir++) {
						int nextx = hqX + dx[dir];
						int nexty = hqY + dy[dir];

						if (nextx < mnX || nextx > mxX || nexty < mnY
								|| nexty > mxY)
							continue;

						int nextIndex = ((nextx & 0x7F) << INDEX_SHIFT)
								+ (nexty & 0x7F) + BC_FIELD_BASE;

						TerrainTile tt = rc.senseTerrainTile(new MapLocation(
								nextx + originX, nexty + originY));

						if (tt == TerrainTile.UNKNOWN
								|| tt == TerrainTile.NORMAL) {
							SharedQueue.addLast(nextIndex);
						}

						rc.broadcast(nextIndex,
								((currentVersion + 1) << 16) + 1);
					}

					// TODO: uncomment
					// for (MapLocation tower : Robot.enemyTowers) {
					// int towerIndex = getIndex(tower.x - originX, tower.y
					// - originY);
					// SharedQueue.addLast(towerIndex);
					// usedV = currentVersion + 1;
					// dist = 0;
					// writeIndexInfo(towerIndex);
					// }

					rc.broadcast(BC_BFS_INITED, 1);

					// System.out.println("BFS INITED " + Clock.getRoundNum()
					// + " in " + (Clock.getBytecodeNum() - start)
					// + " bytecode");
				}
			}

			while (true) {
				// int cnt = 0;
				// int start = Clock.getBytecodeNum();
				if (Clock.getBytecodeNum() >= endTime - 1111) {
					break;
				}
				if (SharedQueue.first == SharedQueue.last) {
					rc.broadcast(BC_BFS_INITED, 0);
					break;
				}

				int currIndex = SharedQueue.pollFirst();
				int nextDist = ((rc.readBroadcast(currIndex)) & 0xFFFF) + 1;

				currIndex -= BC_FIELD_BASE;
				int currx = currIndex >> INDEX_SHIFT;
				int curry = currIndex & INDEX_MASK;
				while (currx < mnX)
					currx += 128;
				while (curry < mnY)
					curry += 128;

				for (int dir = 0; dir < 8; dir++) {
					int nextx = currx + dx[dir];
					int nexty = curry + dy[dir];

					if (nextx < mnX || nextx > mxX || nexty < mnY
							|| nexty > mxY)
						continue;

					int nextIndex = ((nextx & 0x7F) << INDEX_SHIFT)
							+ (nexty & 0x7F) + BC_FIELD_BASE;

					if ((rc.readBroadcast(nextIndex) >> 16) <= currentVersion) {
						TerrainTile tt = rc.senseTerrainTile(new MapLocation(
								nextx + originX, nexty + originY));

						if (tt == TerrainTile.UNKNOWN
								|| tt == TerrainTile.NORMAL) {
							SharedQueue.addLast(nextIndex);
						}

						rc.broadcast(nextIndex, ((currentVersion + 1) << 16)
								+ nextDist);

						// ++cnt;
					}
				}

				// System.out.println("Bytecode for iteration: "
				// + (Clock.getBytecodeNum() - start) + " " + cnt);
			}

			SharedQueue.save();
		}
	}
}

class SharedQueue {
	static int first;
	static int last;
	static final int BEGIN = Pathfinder.BC_QUEUE_BEGIN;
	static final int END = Pathfinder.BC_QUEUE_END;


	/** 5 */
	static void reset() {
		first = last = BEGIN;
	}

	/** 20! */
	static void load() throws GameActionException {
		first = rc.readBroadcast(Pathfinder.BC_QUEUE_FIRST);
		last = rc.readBroadcast(Pathfinder.BC_QUEUE_LAST);
	}

	/** 60! */
	static void save() throws GameActionException {
		rc.broadcast(Pathfinder.BC_QUEUE_FIRST, first);
		rc.broadcast(Pathfinder.BC_QUEUE_LAST, last);
	}

	/** 40 */
	static void addLast(int x) throws GameActionException {
		rc.broadcast(last, x);
		++last;
		if (last == END)
			last = BEGIN;
	}

	/** 20 */
	static int pollFirst() throws GameActionException {
		int ret = rc.readBroadcast(first);
		++first;
		if (first == END)
			first = BEGIN;
		return ret;
	}
}