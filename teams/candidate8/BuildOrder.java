package candidate8;

import battlecode.common.*;
import static battlecode.common.RobotType.*;
import static candidate8.Robot.*;

abstract class BuildOrder {
	static RobotType bo[]=null;
	static int k=0;
	static TileCheck buildRestriction=TileCheck.checker;
	
	abstract boolean build() throws GameActionException;
	
	
	static BuildOrder autoSupply= new BuildOrder(){
		boolean build() throws GameActionException{
			if(Clock.getRoundNum()>50&&rc.senseRobotAtLocation(myHQ).supplyLevel<2500+Clock.getRoundNum()&&rc.getTeamOre()>=SUPPLYDEPOT.oreCost){
				int i=1,sign=1,d=facing.ordinal()+8;
				Direction dir;
				for(;i<=8;d+=i*sign,i++,sign*=-1){
					dir=direction[d%8];
					if(!rc.canBuild(dir,bo[k])||!buildRestriction.check(myLocation.add(dir)))continue;
					Debug.debug_coreAction("Auto building "+bo[k]+".");
					rc.build(dir,SUPPLYDEPOT);
					return true;
				}
				return false;
			}
			if(k==bo.length){
				if(rc.getTeamOre()<bo[k].oreCost+200){
					return standardBuild.build();
				}
			}
			else 
				return standardBuild.build();
			return false;
		}
	};
	
	static BuildOrder standardBuild=new BuildOrder(){
		boolean build() throws GameActionException{
			if(rc.getTeamOre()<bo[k].oreCost)return false;
			int i=1,sign=1,d=facing.ordinal()+8;
			Direction dir;
			for(;i<=8;d+=i*sign,i++,sign*=-1){
				dir=direction[d%8];
				if(!rc.canBuild(dir,bo[k])||!buildRestriction.check(myLocation.add(dir)))continue;
				Debug.debug_coreAction("Standard building "+bo[k]+".");
				rc.build(dir,bo[k++]);
				if(k==bo.length)k--;
				return true;
			}
			return false;
		}
	};
	
	static int phasing=1000;
	
	static BuildOrder phaseOutFast=new BuildOrder(){
		boolean build() throws GameActionException{
			phasing++;
			if(phasing>100){
				if(standardSpawn.build()){
					phasing-=100;
					return true;
				}
			}
			return false;
		}
	};
	

	static BuildOrder phaseOutSlow=new BuildOrder(){
		boolean build() throws GameActionException{
			phasing++;
			if(phasing>40){
				if(standardSpawn.build()){
					phasing-=40;
					return true;
				}
			}
			return false;
		}
	};
	

	static BuildOrder standardSpawn=new BuildOrder(){
		boolean build() throws GameActionException{
			if(rc.getTeamOre()<bo[k].oreCost)return false;
			int i=1,sign=1,d=facing.ordinal()+8;
			Direction dir;
			for(;i<=8;d+=i*sign,i++,sign*=-1){
				dir=direction[d%8];
				if(!rc.canSpawn(dir,bo[k]))continue;
				Debug.debug_coreAction("Standard spawning "+bo[k]+".");
				rc.spawn(dir,bo[k]);
				k++;
				if(k==bo.length)k--;
				return true;
			}
			return false;
		}
	};
	
	static class LuxurySpawn extends BuildOrder{
		static int oreCap;
		LuxurySpawn(int ore){
			oreCap=ore;
		}
		@Override
		boolean build() throws GameActionException{
			if(rc.getTeamOre()<oreCap&&k==bo.length-1)return false;
			return standardSpawn.build();
		}
	};

	static class LuxuryBuild extends BuildOrder{
		static int oreCap;
		LuxuryBuild(int ore){
			oreCap=ore;
		}
		@Override
		boolean build() throws GameActionException{
			if(rc.getTeamOre()<oreCap&&k==bo.length-1&&rc.senseRobotAtLocation(myHQ).supplyLevel>=2500+Clock.getRoundNum())return false;
			return autoSupply.build();
		}
	};
}
