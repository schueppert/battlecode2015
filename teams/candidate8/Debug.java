package candidate8;

import battlecode.common.*;
import static battlecode.common.RobotType.*;
import static candidate8.Robot.*;

public class Debug {
	static MapLocation myLocation;
	static void debug_facing(){
		rc.setIndicatorString(1,"drawing a stirng!");
		rc.setIndicatorLine(myLocation,myLocation.add(facing),255,255,255);
	}
	static void debug_setLocation(MapLocation l){
		myLocation=l;
	}

	static int lastBC;
	
	static void debug_zeroBC(){
		lastBC=Clock.getBytecodeNum();
	}
	
	static void debug_profileBC(){
		rc.setIndicatorString(2,"Bytecode used for funk: "+(Clock.getBytecodeNum()-lastBC));
	}
	
	static void debug_target(MapLocation target){
		rc.setIndicatorLine(myLocation, target, 0, 0,255);
	}
	
	static void debug_writeString1(String s){
		rc.setIndicatorString(0, s);
	}
	static void debug_writeString2(String s){
		rc.setIndicatorString(1, s);
	}
	static void debug_coreAction(String s){
		rc.setIndicatorString(2, s);
	}
}
