package fireants1;

import battlecode.common.*;

public class RobotPlayer {
    static RobotController rc;
    static RobotType type;
    static Team myTeam;
    static Team enemyTeam;
    static MapLocation midway;
    static int cohort;
    static MapLocation target;
    static int combatMode;
    static RobotInfo[] myBots;
    static RobotInfo[] enemyBots;
    static Direction[] directions = {Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST, Direction.NORTH_EAST, Direction.SOUTH_EAST, Direction.SOUTH_WEST, Direction.NORTH_WEST};
    static int[][] map;

    //Variable setup and robot type dispatch
    public static void run(RobotController thisRC) {
        try {
            //System.out.println("Hello World");
            rc = thisRC;
            type = rc.getType();
            myTeam = rc.getTeam();
            enemyTeam = myTeam.opponent();
            midway = midway();
            if (Clock.getRoundNum() < 800) cohort = 1;
            else cohort = 2;
            switch (type) {
                case BEAVER:
                    beaver();
                    break;
                case TOWER:
                    tower();
                    break;
                case HELIPAD:
                    helipad();
                    break;
                case SUPPLYDEPOT:
                    supplyDepot();
                    break;
                case HANDWASHSTATION:
                    supplyDepot();
                    break;
                case DRONE:
                    drone();
                    break;
                case HQ:
                    hq();
                    break;
            }

        } catch (GameActionException e) {
            System.out.println("Unexpected exception");
            e.printStackTrace();
        }
    }

    //Run sequence for BEAVER
    static void beaver() throws GameActionException {
        while (true) {
            myBots = rc.senseNearbyRobots(rc.senseHQLocation(), RobotType.HQ.attackRadiusSquared, myTeam);
            attackNearestEnemy();
            if (count(RobotType.HELIPAD, myBots) < 1) {
                build(RobotType.HELIPAD);
            }
            if (count(RobotType.SUPPLYDEPOT, myBots) < 1) {
                build(RobotType.SUPPLYDEPOT);
            }
            if (count(RobotType.HELIPAD, myBots) < 2 && Clock.getRoundNum() > 300) {
                build(RobotType.HELIPAD);
            }
            if (Clock.getRoundNum() > 1800) build(RobotType.HANDWASHSTATION);
            mine();
            rc.yield();
        }
    }

    //Run sequence for TOWER
    static void tower() throws GameActionException {
        while (true) {
            attackNearestEnemy();
            rc.yield();
        }
    }

    //Run sequence for HELIPAD
    static void helipad() throws GameActionException {
        while (true) {
            if (Clock.getRoundNum() < 1800) spawn(RobotType.DRONE);
            rc.yield();
        }
    }

    //Run sequence for SUPPLYDEPOT
    static void supplyDepot() throws GameActionException {
        while (true) rc.yield();
    }

    //Run sequence for DRONE
    static void drone() throws GameActionException {
        buildMap();
        while (true) {
            myBots = rc.senseNearbyRobots(type.sensorRadiusSquared, myTeam);
            enemyBots = rc.senseNearbyRobots(type.sensorRadiusSquared, enemyTeam);
            int oldCombatMode = combatMode;
            readStrategy();
            if (oldCombatMode == 2 && combatMode == 0) buildMap();
            if (enemyBots.length > 0) {
                attackNearestEnemy();
                target = nearest(enemyBots).location;
                if (combatMode == 0) combatMode = 1;
            }
//            rc.setIndicatorString(1, "cohort: " + Integer.toString(cohort));
//            rc.setIndicatorString(2, "combatMode: " + Integer.toString(combatMode));
            move();
            rc.yield();
        }
    }

    //Run sequence for HQ
    static void hq() throws GameActionException {
        while (true) {
            myBots = rc.senseNearbyRobots(1000, myTeam);
            enemyBots = rc.senseNearbyRobots(1000, enemyTeam);
            if (count(RobotType.BEAVER, myBots) < 3) {
                spawn(RobotType.BEAVER);
            }
            hqAttack();
            //rc.setIndicatorString(1,"HQ Attackradius:"+Integer.toString(hqAttackRadius(myTeam)));
            supplyTransfer();
            setStrategy();
            rc.yield();
        }
    }

    //Set strategy
    static void setStrategy() throws GameActionException {
        MapLocation target1, target2;
        target1 = rc.senseEnemyHQLocation();
        if (rc.senseEnemyTowerLocations().length == 0) target2 = rc.senseEnemyHQLocation();
        else target2 = nearest(rc.senseEnemyTowerLocations());
        int combatMode1 = 0;
        int combatMode2 = 0;
//        rc.setIndicatorString(1, "Strength per tower at target2: " + Integer.toString(strengthPerTower(target2)));
        if (strengthPerTower(target2) > 20000) {
            combatMode2 = 2;
        }
        if (target2 == rc.senseEnemyHQLocation() && strengthPerTower(target2) > 20000) {
            combatMode1 = 2;
            combatMode2 = 2;
        }
        MapLocation localDanger = localDanger();
        if (localDanger != null) {
            target2 = localDanger;
            combatMode2 = 0;
        }
        broadcastStrategy(target1, target2, combatMode1, combatMode2);
    }


    //Supply transfer
    static void supplyTransfer() throws GameActionException {
        RobotInfo[] bots = rc.senseNearbyRobots(GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, myTeam);
        for (RobotInfo bot : bots) {
            if (bot.type == RobotType.DRONE && bot.supplyLevel == 0)
                rc.transferSupplies(Math.min((int) rc.getSupplyLevel(), 7500), bot.location);
        }
    }

    //Attack nearest enemy
    static void attackNearestEnemy() throws GameActionException {
        RobotInfo[] enemies = rc.senseNearbyRobots(type.attackRadiusSquared, enemyTeam);
        if (enemies.length > 0 && rc.isWeaponReady()) rc.attackLocation(nearest(enemies).location);
    }

    //HQ attack nearest enemy (allowing for buffs)
    static void hqAttack() throws GameActionException {
        RobotInfo[] enemies = rc.senseNearbyRobots(hqAttackRadius(myTeam), enemyTeam);
        if (enemies.length > 0) {
            MapLocation attackSpot = nearest(enemies).location;
            if (rc.senseEnemyHQLocation().distanceSquaredTo(attackSpot) > 35)
                attackSpot.add(attackSpot.directionTo(rc.senseHQLocation()));
            if (rc.isWeaponReady() && rc.canAttackLocation(attackSpot)) rc.attackLocation(attackSpot);
        }
    }

    //Get nearest robot from list of robots
    static RobotInfo nearest(RobotInfo[] bots) {
        RobotInfo nearest = bots[0];
        int distance;
        for (RobotInfo bot : bots) {
            distance = rc.getLocation().distanceSquaredTo(nearest.location);
            if (rc.getLocation().distanceSquaredTo(bot.location) < distance) nearest = bot;
        }
        return nearest;
    }

    //Get nearest location from list of MapLocations
    static MapLocation nearest(MapLocation[] locations) {
        MapLocation nearest = locations[0];
        int distance;
        for (MapLocation location : locations) {
            distance = rc.getLocation().distanceSquaredTo(nearest);
            if (rc.getLocation().distanceSquaredTo(location) < distance) nearest = location;
        }
        return nearest;
    }

    //Spawn a new robot of a given type
    static void spawn(RobotType type) throws GameActionException {
        if (rc.isCoreReady()) for (Direction direction : directions) {
            if (rc.canSpawn(direction, type)) {
                rc.spawn(direction, type);
                break;
            }
        }
    }

    //Build a new robot of a given type
    static void build(RobotType type) throws GameActionException {
        if (rc.isCoreReady()) for (Direction direction : directions)
            if (rc.canBuild(direction, type)) {
                rc.build(direction, type);
                break;
            }
    }

    //Get count of given unit type
    static int count(RobotType type, RobotInfo[] bots) {
        int count = 0;
        for (RobotInfo bot : bots) if (bot.type == type) count += 1;
        return count;
    }

    //Build map of tower coverage - 1 My HQ, 2 My Towers, 3 Enemy HQ, 4 Enemy Towers
    static void buildMap() {
        map = new int[GameConstants.MAP_MAX_WIDTH + 20][GameConstants.MAP_MAX_HEIGHT + 20];
        setMapValues(rc.senseHQLocation(), hqAttackRadius(myTeam), 1);
        setMapValues(rc.senseEnemyHQLocation(), hqAttackRadius(enemyTeam), 3);
        setMapValues(rc.senseTowerLocations(), RobotType.TOWER.attackRadiusSquared, 2);
        setMapValues(rc.senseEnemyTowerLocations(), RobotType.TOWER.attackRadiusSquared, 4);
    }

    static int hqAttackRadius(Team team) {
        int towers;
        if (team == myTeam) towers = rc.senseTowerLocations().length;
        else towers = rc.senseEnemyTowerLocations().length;
        if (towers <= 1) return 24;
        if (towers <= 4) return 35;
        return 37;
    }

    //Set map values for array of locations
    static void setMapValues(MapLocation[] locations, int range, int value) {
        for (MapLocation location : locations) setMapValues(location, range, value);
    }

    //Set map values for a single location
    static void setMapValues(MapLocation location, int range, int value) {
        MapLocation[] squares = MapLocation.getAllMapLocationsWithinRadiusSq(location, range);
        for (MapLocation square : squares) {
            square = removeOffset(square);
            map[square.x][square.y] = value;
        }
    }

    //Remove random offset from MapLocation values
    static MapLocation removeOffset(MapLocation location) {
        return location.add(GameConstants.MAP_MAX_WIDTH / 2 + 5 - midway.x, GameConstants.MAP_MAX_HEIGHT / 2 + 5 - midway.y);
    }

    //Add random offset from MapLocation values
    static MapLocation addOffset(MapLocation location) {
        return location.add(midway.x - GameConstants.MAP_MAX_WIDTH / 2 - 5, midway.y - GameConstants.MAP_MAX_HEIGHT / 2 - 5);
    }

    //Return map value of a given location - 1 My HQ, 2 My Towers, 3 Enemy HQ, 4 Enemy Towers
    static int map(MapLocation location) {
        MapLocation coordinates = removeOffset(location);
        return map[coordinates.x][coordinates.y];
    }

    //Calculate midway location
    static MapLocation midway() {
        MapLocation hq = rc.senseHQLocation();
        MapLocation enemyHQ = rc.senseEnemyHQLocation();
        int x = (hq.x + enemyHQ.x) / 2;
        int y = (hq.y + enemyHQ.y) / 2;
        return new MapLocation(x, y);
    }

    //Move to Target
    static void move() throws GameActionException {
        if (!rc.isCoreReady()) return;
        int bestScore = 0;
        Direction bestDirection = Direction.NONE;
        for (Direction direction : directions) {
            int score = score(direction);
            if (score > bestScore) {
                bestScore = score;
                bestDirection = direction;
            }
        }
        if (bestDirection != Direction.NONE) rc.move(bestDirection);
    }

    //Score the quality of a move towards a target
    static int score(Direction direction) {
        if (!rc.canMove(direction)) return 0;
        if (combatMode < 2 && map(rc.getLocation().add(direction)) > 2) return 0;
        int score = rc.getLocation().distanceSquaredTo(target) - rc.getLocation().add(direction).distanceSquaredTo(target);
        if (direction.isDiagonal()) score -= 1;
        if (combatMode != 1) return score;
        for (RobotInfo bot : enemyBots) {
            if (rc.getLocation().add(direction).distanceSquaredTo(bot.location) <= bot.type.attackRadiusSquared)
                score -= bot.type.attackPower;
        }
        if (rc.getLocation().add(direction).distanceSquaredTo(target) <= type.attackRadiusSquared)
            score += type.attackPower;
        for (RobotInfo bot : enemyBots) {
            if (rc.getLocation().add(direction).distanceSquaredTo(bot.location) <= 10) score -= 1;
        }
        for (RobotInfo bot : myBots) {
            if (rc.getLocation().add(direction).distanceSquaredTo(bot.location) <= 2) score += 1;
        }
        return score;
    }

    //Convert MapLocation to int for broadcasting
    static int locationToInt(MapLocation location) {
        location = removeOffset(location);
        return location.x + location.y * GameConstants.MAP_MAX_WIDTH;
    }

    //Convert broadcast int to MapLocation
    static MapLocation intToLocation(int data) {
        int x = data % GameConstants.MAP_MAX_WIDTH;
        int y = (data - x) / GameConstants.MAP_MAX_WIDTH;
        return addOffset(new MapLocation(x, y));
    }

    //Broadcast strategy - reads target and combatMode
    static void broadcastStrategy(MapLocation target1, MapLocation target2, int combatMode1, int combatMode2) throws GameActionException {
        rc.broadcast(1, locationToInt(target1));
        rc.broadcast(2, locationToInt(target2));
        rc.broadcast(3, combatMode1);
        rc.broadcast(4, combatMode2);
    }

    //Read broadcast strategy - sets target and combatMode
    static void readStrategy() throws GameActionException {
        target = intToLocation(rc.readBroadcast(cohort));
        combatMode = rc.readBroadcast(cohort + 2);
        if (cohort == 1 && rc.readBroadcast(4) == 2) {
            MapLocation target2 = intToLocation(rc.readBroadcast(2));
            if (rc.getLocation().distanceSquaredTo(target2) < rc.getLocation().distanceSquaredTo(target))
                cohort = 2;
        }
    }

    //Simple mining algorithm
    static void mine() throws GameActionException {
        if (!rc.isCoreReady()) return;
        double currentOre = rc.senseOre(rc.getLocation());
        double bestOre = currentOre;
        Direction bestDirection = Direction.NONE;
        for (Direction direction : directions) {
            double ore = rc.senseOre(rc.getLocation().add(direction));
            if (ore > bestOre && ore > 5 * currentOre && rc.canMove(direction)) {
                bestOre = ore;
                bestDirection = direction;
            }
        }
        if (bestDirection != Direction.NONE) rc.move(bestDirection);
        else rc.mine();
    }

    //Measure of strength per tower at location
    static int strengthPerTower(MapLocation location) {
        return (int) strength(location, myTeam) / enemyTowerCount(location);
    }

    //My Army strength at location
    static int strength(MapLocation location, Team team) {
        int range = 40;
        RobotInfo[] bots = rc.senseNearbyRobots(location, range, team);
        if (bots.length == 0) return 1;
        int health = 0;
        int attack = 0;
        for (RobotInfo bot : bots) {
            health += bot.health;
            if (bot.type == RobotType.LAUNCHER) attack += 20 / 6;
            else attack += bot.type.attackPower / bot.type.attackDelay;
        }
        return (int) health * attack;
    }

    //Enemy tower count at location (including HQ)
    static int enemyTowerCount(MapLocation location) {
        MapLocation[] towers = rc.senseEnemyTowerLocations();
        if (towers.length == 0) return 3;
        int count = 0;
        int range = 10;
        for (MapLocation tower : towers) {
            if (tower.distanceSquaredTo(location) < range) count += 1;
        }
        if (rc.senseEnemyHQLocation().distanceSquaredTo(location) < range) count += 3;
        return count;
    }

    static MapLocation localDanger() {
        MapLocation[] towers = rc.senseTowerLocations();
        MapLocation dangerSpot = rc.senseHQLocation();
        int dangerScore = strength(rc.senseHQLocation(), enemyTeam);
        for (MapLocation tower : towers) {
            int score = strength(tower, enemyTeam);
            if (score > dangerScore) {
                dangerScore = score;
                dangerSpot = tower;
            }
        }
        if (dangerScore > 10000) return dangerSpot;
        else return null;
    }

}
