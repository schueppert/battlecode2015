package fireants5;

import battlecode.common.*;

/**
 * Created by schueppert on 1/25/15.
 */
public class RobotObject {
    static RobotController rc;

    static void run(RobotController anRC) {
        rc = anRC;
        start();
    }

    static void start() {


    }

    static void move(Direction direction) {
        try {
            if (rc.canMove(direction)) rc.move(direction);
            ;
        } catch (GameActionException e) {
            System.out.print(rc.toString() + " cannot move " + direction.toString());
        }
    }

    static void attackLocation(MapLocation mapLocation) {
        try {
            if(rc.canAttackLocation(mapLocation)) rc.attackLocation(mapLocation);
        } catch (GameActionException e) {
            System.out.print(rc.toString() + " cannot attack " + mapLocation.toString());
        }

    }

    static void broadcast(int i, int i1) {
        try {
            rc.broadcast(i, i1);
        } catch (GameActionException e) {
            System.out.print(rc.toString() + " cannot broadcast on channel " + Integer.toString(i));
        }
    }

    static int readBroadcast(int i) {
        try {
            return rc.readBroadcast(i);
        } catch (GameActionException e) {
            System.out.print(rc.toString() + " cannot read broadcast on channel " + Integer.toString(i));
            return 0;
        }
    }

    static void transferSupplies(int i, MapLocation mapLocation) {
        try {
            rc.transferSupplies(i, mapLocation);
        } catch (GameActionException e) {
            System.out.print(rc.toString() + " cannot transfer supplies to " + mapLocation.toString());
        }
    }

    static void mine() {
        try {
            if(rc.canMine()) rc.mine();
        } catch (GameActionException e) {
            System.out.print(rc.toString() + " cannot mine");
        }
    }

    static void spawn(Direction direction, RobotType robotType) {
        try {
            rc.spawn(direction, robotType);
        } catch (GameActionException e) {
            System.out.print(rc.toString() + " cannot spawn " + robotType.toString() + " to " + direction.toString());
        }
    }

    static void build(Direction direction, RobotType robotType) {
        try {
            rc.build(direction, robotType);
        } catch (GameActionException e) {
            System.out.print(rc.toString() + " cannot build " + robotType.toString() + " to " + direction.toString());
        }
    }

}
