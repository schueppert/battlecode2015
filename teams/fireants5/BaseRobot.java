package fireants5;

import battlecode.common.*;

/**
 * Created by schueppert on 1/25/15.
 */
public class BaseRobot implements RobotController {
    RobotController rc;


    public void run(RobotController anRC) {
        rc = anRC;

    }


    @Override
    public int getRoundLimit() {
        return rc.getRoundLimit();
    }

    @Override
    public double getTeamOre() {
        return rc.getTeamOre();
    }

    @Override
    public int getID() {
        return rc.getID();
    }

    @Override
    public Team getTeam() {
        return rc.getTeam();
    }

    @Override
    public RobotType getType() {
        return rc.getType();
    }

    @Override
    public MapLocation getLocation() {
        return rc.getLocation();
    }

    @Override
    public double getCoreDelay() {
        return rc.getCoreDelay();
    }

    @Override
    public double getWeaponDelay() {
        return rc.getWeaponDelay();
    }

    @Override
    public double getHealth() {
        return rc.getHealth();
    }

    @Override
    public double getSupplyLevel() {
        return rc.getSupplyLevel();
    }

    @Override
    public int getXP() {
        return rc.getXP();
    }

    @Override
    public int getMissileCount() {
        return rc.getMissileCount();
    }

    @Override
    public boolean isBuildingSomething() {
        return rc.isBuildingSomething();
    }

    @Override
    public MapLocation senseHQLocation() {
        return rc.senseHQLocation();
    }

    @Override
    public MapLocation senseEnemyHQLocation() {
        return rc.senseEnemyHQLocation();
    }

    @Override
    public MapLocation[] senseTowerLocations() {
        return rc.senseTowerLocations();
    }

    @Override
    public MapLocation[] senseEnemyTowerLocations() {
        return rc.senseEnemyTowerLocations();
    }

    @Override
    public TerrainTile senseTerrainTile(MapLocation mapLocation) {
        return rc.senseTerrainTile(mapLocation);
    }

    @Override
    public boolean canSenseLocation(MapLocation mapLocation) {
        return rc.canSenseLocation(mapLocation);
    }

    @Override
    public boolean isLocationOccupied(MapLocation mapLocation) throws GameActionException {
        return rc.isLocationOccupied(mapLocation);
    }

    @Override
    public RobotInfo senseRobotAtLocation(MapLocation mapLocation) throws GameActionException {
        return rc.senseRobotAtLocation(mapLocation);
    }

    @Override
    public boolean canSenseRobot(int i) {
        return rc.canSenseRobot(i);
    }

    @Override
    public RobotInfo senseRobot(int i) throws GameActionException {
        return rc.senseRobot(i);
    }

    @Override
    public RobotInfo[] senseNearbyRobots() {
        return rc.senseNearbyRobots();
    }

    @Override
    public RobotInfo[] senseNearbyRobots(int i) {
        return rc.senseNearbyRobots(i);
    }

    @Override
    public RobotInfo[] senseNearbyRobots(int i, Team team) {
        return rc.senseNearbyRobots(i, team);
    }

    @Override
    public RobotInfo[] senseNearbyRobots(MapLocation mapLocation, int i, Team team) {
        return rc.senseNearbyRobots(mapLocation, i, team);
    }

    @Override
    public boolean isCoreReady() {
        return rc.isCoreReady();
    }

    @Override
    public boolean isWeaponReady() {
        return rc.isWeaponReady();
    }

    @Override
    public boolean isPathable(RobotType robotType, MapLocation mapLocation) {
        return rc.isPathable(robotType, mapLocation);
    }

    @Override
    public boolean canMove(Direction direction) {
        return rc.canMove(direction);
    }

    @Override
    public void move(Direction direction) {
        try {
            rc.move(direction);
        } catch (GameActionException e) {
            System.out.print(e.toString());
        }
    }

    @Override
    public boolean canAttackLocation(MapLocation mapLocation) {
        return rc.canAttackLocation(mapLocation);
    }

    @Override
    public void attackLocation(MapLocation mapLocation) {
        try {
            rc.attackLocation(mapLocation);
        } catch (GameActionException e) {
            System.out.print(e.toString());
        }
    }

    @Override
    public void explode() {
        try {
            rc.explode();
        } catch (GameActionException e) {
            System.out.print(e.toString());
        }
    }

    @Override
    public boolean hasCommander() {
        return false;
    }

    @Override
    public void castFlash(MapLocation mapLocation) throws GameActionException {

    }

    @Override
    public boolean hasLearnedSkill(CommanderSkillType commanderSkillType) throws GameActionException {
        return false;
    }

    @Override
    public int getFlashCooldown() throws GameActionException {
        return 0;
    }

    @Override
    public void broadcast(int i, int i1) throws GameActionException {

    }

    @Override
    public int readBroadcast(int i) throws GameActionException {
        return 0;
    }

    @Override
    public void transferSupplies(int i, MapLocation mapLocation) throws GameActionException {

    }

    @Override
    public boolean canMine() {
        return false;
    }

    @Override
    public double senseOre(MapLocation mapLocation) {
        return 0;
    }

    @Override
    public void mine() throws GameActionException {

    }

    @Override
    public boolean canLaunch(Direction direction) {
        return false;
    }

    @Override
    public void launchMissile(Direction direction) throws GameActionException {

    }

    @Override
    public DependencyProgress checkDependencyProgress(RobotType robotType) {
        return null;
    }

    @Override
    public boolean hasSpawnRequirements(RobotType robotType) {
        return false;
    }

    @Override
    public boolean canSpawn(Direction direction, RobotType robotType) {
        return false;
    }

    @Override
    public void spawn(Direction direction, RobotType robotType) throws GameActionException {

    }

    @Override
    public boolean hasBuildRequirements(RobotType robotType) {
        return false;
    }

    @Override
    public boolean canBuild(Direction direction, RobotType robotType) {
        return false;
    }

    @Override
    public void build(Direction direction, RobotType robotType) throws GameActionException {

    }

    @Override
    public void yield() {

    }

    @Override
    public void disintegrate() {

    }

    @Override
    public void resign() {

    }

    @Override
    public void setTeamMemory(int i, long l) {

    }

    @Override
    public void setTeamMemory(int i, long l, long l1) {

    }

    @Override
    public long[] getTeamMemory() {
        return new long[0];
    }

    @Override
    public void setIndicatorString(int i, String s) {

    }

    @Override
    public void setIndicatorDot(MapLocation mapLocation, int i, int i1, int i2) {

    }

    @Override
    public void setIndicatorLine(MapLocation mapLocation, MapLocation mapLocation1, int i, int i1, int i2) {

    }

    @Override
    public long getControlBits() {
        return 0;
    }

    @Override
    public void addMatchObservation(String s) {

    }

    @Override
    public void breakpoint() {

    }
}
