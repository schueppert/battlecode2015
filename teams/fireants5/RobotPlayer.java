package fireants5;

import battlecode.common.*;

import java.util.Arrays;
import java.util.Random;


public class RobotPlayer {

    static RobotController rc;
    static Random random;
    static MapLocation hq;
    static MapLocation enemyHQ;
    static MapLocation[] towers;
    static MapLocation[] enemyTowers;
    static Direction orientation;
    static int[] count;

    public static void run(RobotController anRC) throws GameActionException {
        setData(anRC);
        if (rc.getType() == RobotType.HQ) hq();
        if (rc.getType() == RobotType.SOLDIER || rc.getType() == RobotType.COMMANDER) soldier();
        while (true) {
            updateData();
            if (rc.getType().needsSupply()) transferSupply();
            if (rc.getType().canAttack() && rc.isWeaponReady()) attack();
            if (rc.getType().canBuild()) build();
            if (rc.getType().canSpawn()) spawn();
            if (rc.getType().canMove()) move();
            rc.yield();
        }
    }

    static void setData(RobotController anRC) {
        rc = anRC;
        random = new Random(rc.getID());
        hq = rc.senseHQLocation();
        enemyHQ = rc.senseEnemyHQLocation();
        towers = rc.senseTowerLocations();
        enemyTowers = rc.senseEnemyTowerLocations();
        orientation = directions[rc.getID() % 8];
        count = new int[RobotType.values().length];
        makePoints();
    }

    static void hq() throws GameActionException {
        while (true) {
            updateCount();
            transferSupply();
            if (count[RobotType.BEAVER.ordinal()] == 0) safeSpawn(RobotType.BEAVER);
            RobotInfo[] enemies = rc.senseNearbyRobots(hqAattackRadius(rc.getType()), rc.getTeam().opponent());
            if (enemies.length == 0) return;
            RobotInfo weakestEnemy = enemies[0];
            for (RobotInfo e : enemies)
                if (e.health < weakestEnemy.health) {
                    weakestEnemy = e;
                    rc.broadcast(e.ID, Clock.getRoundNum());
                }
            MapLocation attackSpot = weakestEnemy.location;
            if (rc.getLocation().distanceSquaredTo(attackSpot) > 35)
                attackSpot = attackSpot.add(attackSpot.directionTo(hq));
            if (rc.isWeaponReady() && rc.canAttackLocation(attackSpot)) rc.attackLocation(attackSpot);
            rc.yield();
        }
    }

    static int hqAattackRadius(RobotType type) {
        if (towers.length < 2) return 24;
        else if (towers.length < 5) return 35;
        else return squadd(35, 2);
    }

    static void soldier() throws GameActionException {
        while (true) {
            buildTowerMap();
            transferSupply();
            attack();
            soldierMove();
            rc.yield();
        }
    }

    static void updateData() throws GameActionException {
        if (rc.getType().canBuild() || rc.getType().canSpawn()) updateCount();
        if (rc.getType().canMove()) buildTowerMap();
    }

    static void transferSupply() throws GameActionException {
        if (rc.getSupplyLevel() < 200) return;
        RobotInfo[] friends = rc.senseNearbyRobots(GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, rc.getTeam());
        for (RobotInfo f : friends) {
            if (f.type.needsSupply() && (rc.getSupplyLevel() - f.supplyLevel >= 200)) {
                rc.transferSupplies((int) ((rc.getSupplyLevel() - f.supplyLevel) / 2), f.location);
                return;
            }
        }
    }

    static void attack() throws GameActionException {
        RobotInfo[] enemies = rc.senseNearbyRobots(rc.getType().attackRadiusSquared, rc.getTeam().opponent());
        if (enemies.length == 0) return;
        RobotInfo weakestEnemy = enemies[0];
        for (RobotInfo e : enemies)
            if (e.health < weakestEnemy.health) {
                weakestEnemy = e;
                rc.broadcast(e.ID, Clock.getRoundNum());
            }
        if (rc.isWeaponReady() && rc.canAttackLocation(weakestEnemy.location)) rc.attackLocation(weakestEnemy.location);
    }

    static final int MINERFACTORY = RobotType.MINERFACTORY.ordinal();
    static final int BARRACKS = RobotType.BARRACKS.ordinal();
    static final int SUPPLYDEPOT = RobotType.SUPPLYDEPOT.ordinal();
    static final int TECHNOLOGYINSTITUTE = RobotType.TECHNOLOGYINSTITUTE.ordinal();
    static final int TRAININGFIELD = RobotType.TRAININGFIELD.ordinal();
    static final int SOLDIER = RobotType.SOLDIER.ordinal();

    static void build() throws GameActionException {
        RobotType order = null;
        double supply = (100 * (2 + Math.pow(count[SUPPLYDEPOT], 0.6)));
        if (count[MINERFACTORY] < 1) order = RobotType.MINERFACTORY;
        else if (count[BARRACKS] < 2) order = RobotType.BARRACKS;
        else if (upkeep / supply > 0.8) order = RobotType.SUPPLYDEPOT;
        else if (count[TECHNOLOGYINSTITUTE] < 1 && count[SOLDIER] > 20) order = RobotType.TECHNOLOGYINSTITUTE;
        else if (count[TRAININGFIELD] < 1 && count[SOLDIER] > 20) order = RobotType.TRAININGFIELD;
        else if (rc.getTeamOre() > 750) order = RobotType.BARRACKS;
        if (order != null) safeBuild(order);
    }

    static void spawn() throws GameActionException {
        if (rc.getType() == RobotType.MINER.spawnSource && count[RobotType.MINER.ordinal()] < 20) safeSpawn(RobotType.MINER);
        if (rc.getType() == RobotType.SOLDIER.spawnSource && rc.getTeamOre() > 0) safeSpawn(RobotType.SOLDIER);
        if (rc.getType() == RobotType.COMMANDER.spawnSource && count[RobotType.COMMANDER.ordinal()] < 1) safeSpawn(RobotType.COMMANDER);
    }

    static void move() throws GameActionException {
        if (rc.getType() == RobotType.BEAVER && !canBuild()) safeMove(rc.getLocation().directionTo(hq).opposite());
        if (rc.getType() == RobotType.MINER) {
            Direction d = pathToOre(rc.getLocation());
            if (d != Direction.NONE) safeMove(d);
        }
        if (rc.isCoreReady()) rc.mine();
    }

    static Direction pathToOre(MapLocation location) {
        double currentOre = rc.senseOre(location);
        double bestOre = currentOre;
        Direction bestDirection = Direction.NONE;
        for (Direction d : directions) {
            double newOre = rc.senseOre(location.add(d));
            if (newOre > bestOre) {
                bestOre = newOre;
                bestDirection = d;
            }
        }
        return (bestOre > 2 * currentOre) ? bestDirection : Direction.NONE;
    }
    
    static double upkeep = 0;

    static void updateCount() {
        if (Clock.getRoundNum() > 500 && Clock.getRoundNum() % 10 != 0) return;
        count = new int[RobotType.values().length];
        upkeep = 0;
        RobotInfo[] friends = rc.senseNearbyRobots(120 * 120 * 2, rc.getTeam());
        for (RobotInfo f : friends) {
            count[f.type.ordinal()] += 1;
            upkeep += f.type.supplyUpkeep;
        }
    }

    static int[][] towerMap = new int[120][120];
    static int towerNumber = -1;
    static Boolean mapComplete = false;
    static MapLocation origin;

    static void buildTowerMap() {
        if (mapComplete) return;
        origin = new MapLocation(hq.x + (enemyHQ.x - hq.x) / 2 - 60, hq.y + (enemyHQ.y - hq.y) / 2 - 60);
        MapLocation location;
        int radius;
        if (towerNumber == -1) {
            location = enemyHQ;
            radius = hqAattackRadius(RobotType.HQ);
        } else {
            location = enemyTowers[towerNumber];
            radius = hqAattackRadius(RobotType.TOWER);
        }
        for (MapLocation l : MapLocation.getAllMapLocationsWithinRadiusSq(location, radius)) {
            MapLocation t = trans(l);
            towerMap[t.x][t.y] = 1;
        }
        towerNumber++;
        if (towerNumber == enemyTowers.length) mapComplete = true;
    }

    static MapLocation trans(MapLocation location) {
        return new MapLocation(location.x - origin.x, location.y - origin.y);
    }

    static MapLocation itrans(MapLocation location) {
        return new MapLocation(location.x + origin.x, location.y + origin.y);
    }

    static Direction[][] points = new Direction[8][8];
    static Direction[] directions = Arrays.copyOfRange(Direction.values(), 0, 8);
    static int[] preference = {0, 1, -1, 2, -2, 3, -3, 4};

    static void makePoints() {
        int handedness = (rc.getID() % 2 == 0) ? 1 : -1;
        for (int i = 0; i < directions.length; i++)
            for (int j = 0; j < preference.length; j++)
                points[i][j] = directions[(i + preference[j] * handedness + 8) % 8];
    }

    static Boolean isWhite(MapLocation location) {
        return ((location.x + location.y) % 2 == 0) ? true : false;
    }

    static int squadd(int a, int b) {
        if (b > 0) return (int) ((a + b + 2 * Math.sqrt(a * b)));
        else return (int) ((a + b - 2 * Math.sqrt(-a * b)));
    }

    static void safeBuild(RobotType type) throws GameActionException {
        for (Direction d : points[rc.getLocation().directionTo(enemyHQ).opposite().ordinal()]) {
            MapLocation location = rc.getLocation().add(d);
            if (isWhite(location)) continue;
            if (rc.canBuild(d, type) && rc.isCoreReady()) {
                rc.build(d, type);
                int value = count[type.ordinal()];
                count[type.ordinal()] = value + 1;
                break;
            }
        }
    }

    static void safeSpawn(RobotType type) throws GameActionException {
        Direction target = rc.getLocation().directionTo(enemyHQ).opposite();
        for (Direction d : points[0]) {
            if (rc.canSpawn(d, type) && rc.isCoreReady()) {
                rc.spawn(d, type);
                int value = count[type.ordinal()];
                count[type.ordinal()] = value + 1;
                break;
            }
        }
    }

    static void safeMove(Direction target) throws GameActionException {
        for (Direction d : points[target.ordinal()]) {
            MapLocation newLocation = rc.getLocation().add(d);
            if (combatMoves[d.ordinal()] == 1) continue;
            MapLocation transLocation = trans(newLocation);
            if (towerMap[transLocation.x][transLocation.y] == 1) continue;
            if (rc.canMove(d) && rc.isCoreReady()) {
                orientation = d;
                rc.move(d);
                break;
            }
        }
    }

    static Boolean canBuild() {
        for (Direction d : points[0]) {
            if (isWhite(rc.getLocation().add(d))) continue;
            if (rc.canMove(d)) return true;
        }
        return false;
    }


    static int[] combatMoves = new int[directions.length];

    static void soldierMove() throws GameActionException {
        if (Clock.getRoundNum() % 2 == 0) {
            //TODO signalling
        } else {
            combatMoves = new int[directions.length];
            for (RobotInfo e : rc.senseNearbyRobots(25, rc.getTeam().opponent())) {
                if (e.weaponDelay > 1) continue;
                int dist = rc.getLocation().distanceSquaredTo(e.location);
                if (dist > squadd(e.type.attackRadiusSquared, 1)) continue;
                if ((Clock.getRoundNum() - rc.readBroadcast(e.ID)) < 2) continue;
                int dir = rc.getLocation().directionTo(e.location).ordinal();
                combatMoves[dir] = 1;
            }
            safeMove(rc.getLocation().directionTo(enemyHQ));
        }
    }

}
