package fireants5;

import battlecode.common.RobotController;

/**
 * Created by schueppert on 1/25/15.
 */
class Robot extends RobotObject {
    
    static void start(){
        setData();
        while (true){
            updateData();
            transferSupplies();
            attack();
            spawn();
            build();
            move();
        }
        
        
    }
    
    static void setData(){
        
        
    }
    
    static void updateData(){
        
        
    }
    
    static void transferSupplies(){
        
        
    }
    
    static void attack(){
        
        
    }
    
    static void spawn(){
        
        
    }
    
    static void build(){
        
        
    }
    
    static void move(){
        mine();
    }
    
}
