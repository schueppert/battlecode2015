package team239;

import battlecode.common.*;

public class Drone extends Unit {

	Drone(RobotController rcc) {
		super(rcc);
		// TODO Auto-generated constructor stub
	}

	boolean kiting = false;

	@Override
	public void defaultStrat() throws GameActionException {
		int distance = 20;
		MapLocation target = rc.senseEnemyHQLocation();
		if (kiting) {
			facing = facing.opposite();
			kiting = false;
		} else {
			int senserange = 50;
			if (Clock.getRoundNum() - birthTime < 30)
				senserange = 950 - 30 * (Clock.getRoundNum() - birthTime);
			distance = target.distanceSquaredTo(myLoc);
			boolean isMissile = false;
			for (RobotInfo enemy : rc.senseNearbyRobots(senserange, enemyTeam)) {
				if (enemy.location.distanceSquaredTo(myLoc) < distance) {
					target = enemy.location;
					distance = target.distanceSquaredTo(myLoc);
					isMissile = enemy.type == RobotType.MISSILE;
				}
			}
			facing = isMissile ? target.directionTo(myLoc) : myLoc
					.directionTo(target);
		}
		RobotInfo r[] = rc.senseNearbyRobots(myRange, enemyTeam);
		if (r.length > 0) {
			int x = 0;
			int y = 0;
			for (RobotInfo ri : r) {
				x += 360 / (ri.location.distanceSquaredTo(myLoc));
				y += 360 / (ri.location.distanceSquaredTo(myLoc));
			}
			if (x != 0 || y != 0)
				facing = new MapLocation(myLoc.x + x, myLoc.y + y)
						.directionTo(myLoc);
			else
				facing = myLoc.directionTo(myHq);
			kiting = true;
		}
		if (rc.isWeaponReady()) {
			if (r.length > 0) {
				attackSomething();
				facing = r[0].location.directionTo(rc.getLocation());
				kiting = true;
			}
		}
		if (distance > myRange || !isSafe(rc.getLocation())) {
			if (rc.isCoreReady()) {
				if (rc.senseNearbyRobots(target, 24, enemyTeam).length * 3
						+ enemies.length * 3 < rc.senseNearbyRobots(target, 24,
						myTeam).length)
					moveSafe();
				else
					harrass();
			}
		}
	}

	void harrass() throws GameActionException {
		boolean miner = false;
		if (rand.nextFloat() > 0.9) {
			if (rand.nextFloat() > 0.5)
				facing = facing.rotateLeft();
			else
				facing = facing.rotateRight();
		}
		outer: for (int p = 0; p++ < 8; facing = left ? facing.rotateLeft()
				: facing.rotateRight()) {
			if (!rc.canMove(facing))
				continue;
			MapLocation next = rc.getLocation().add(facing);
			for (RobotInfo enemy : enemies) {
				if ((enemy.weaponDelay <= 1 || (enemy.type == RobotType.TANK && enemy.weaponDelay <= 2))
						&& enemy.location.distanceSquaredTo(next) <= enemy.type.attackRadiusSquared) {
					if (enemy.type == RobotType.BEAVER)
						continue;
					if (enemy.type == RobotType.MINER) {
						if (miner)
							continue outer;
						else
							miner = true;
					} else
						continue outer;
				}
			}
			for (MapLocation tower : towers) {
				if (tower.distanceSquaredTo(next) <= RobotType.TOWER.attackRadiusSquared)
					continue outer;
			}
			if (hq.distanceSquaredTo(next) <= HQrange)
				continue outer;
			rc.move(facing);
			return;
		}
		facing = left ? facing.rotateLeft() : facing.rotateRight();
		if (!isSafe(myLoc))
			moveSafe();
	}
}
