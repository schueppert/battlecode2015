package team239;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class Miner extends Unit {

	Miner(RobotController rcc) {
		super(rcc);
	}

	@Override
	public void defaultStrat() throws GameActionException {
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		if (rc.isCoreReady()) {
			mineMove();
		}
	}
}
