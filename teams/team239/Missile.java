package team239;

import battlecode.common.*;

public class Missile extends Unit {

	Missile(RobotController rcc) {
		super(rcc);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void start() {
		while (true) {

			try {
				MapLocation target = rc.senseEnemyHQLocation(), here = rc
						.getLocation();
				int distance = target.distanceSquaredTo(here);
				for (RobotInfo enemy : rc.senseNearbyRobots(50, enemyTeam)) {
					if (enemy.location.distanceSquaredTo(here) < distance) {
						target = enemy.location;
						distance = target.distanceSquaredTo(here);
					}
				}
				facing = here.directionTo(target);
				if (distance > 10 || !isSafe(rc.getLocation())) {
					if (rc.isCoreReady()) {
						move();
					}
				}
				rc.yield();
			} catch (GameActionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
