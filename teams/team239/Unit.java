package team239;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class Unit extends Robot {
	public Unit(RobotController rcc) {
		super(rcc);
	}

	@Override
	public void start() {
		while (true) {
			hq = rc.senseEnemyHQLocation();
			try {
				initializeTurn();
				supply();
				if (globalOrder == 1 && birthTime < 1800) {
					gatherHQ();
				} else if (globalOrder == 2 && birthTime < 1800
						&& rc.getID() % 5 != 0) {
					searchAndDestroy();
				} else
					defaultStrat();
			} catch (GameActionException e) {
				e.printStackTrace();
			}
			rc.yield();
		}
	}

	@Override
	void initializeTurn() throws GameActionException {
		super.initializeTurn();
		if (chance > 0.98)
			left = !left;
		myLoc = rc.getLocation();
		enemies = rc.senseNearbyRobots(35, enemyTeam);
		friends = rc.senseNearbyRobots(24, myTeam);
		towers = rc.senseEnemyTowerLocations();
		if (towers.length >= 5)
			HQrange = 54;
		else if (towers.length >= 2)
			HQrange = 35;
		else
			HQrange = 24;
	}

	public void defaultStrat() throws GameActionException {
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		if (rc.isCoreReady()) {
			moveSafeAll();
		}
	}

	int gatheredTurns = 0;

	void gatherHQ() throws GameActionException {
		gatheredTurns++;
		MapLocation here = rc.getLocation();
		MapLocation target = myHq;
		facing = here.directionTo(target);
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		if (gatheredTurns - 10 < here.distanceSquaredTo(rc.senseHQLocation())) {
			if (rc.isCoreReady())
				moveSafeAll();
		} else {
			if (enemies.length > 0)
				rc.broadcast(0, 2);
		}
	}

	void searchAndDestroy() throws GameActionException {
		MapLocation towers[] = rc.senseEnemyTowerLocations();
		MapLocation target = null;
		MapLocation here = rc.getLocation();
		if (towers.length > 0) {
			float distance = here.distanceSquaredTo(towers[0]);
			for (MapLocation t : towers) {
				float tempdist = here.distanceSquaredTo(t);
				if (tempdist <= distance) {
					target = t;
					tempdist = distance;
				}
			}
		} else
			target = rc.senseEnemyHQLocation();
		facing = here.directionTo(target);
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		if (rc.isCoreReady()) {
			moveSomewhere();
		}
	}

	RobotInfo[] enemies = rc.senseNearbyRobots(24, enemyTeam);
	RobotInfo[] friends = rc.senseNearbyRobots(24, enemyTeam);
	MapLocation[] towers = rc.senseEnemyTowerLocations();
	MapLocation hq = rc.senseEnemyHQLocation();

	protected boolean isSafe(MapLocation next) {

		for (RobotInfo enemy : enemies) {
			if (enemy.type == RobotType.MISSILE
					&& enemy.location.distanceSquaredTo(rc.getLocation()) < 10)
				return false;
			if (enemy.location.distanceSquaredTo(next) <= enemy.type.attackRadiusSquared)
				return false;
		}
		for (MapLocation tower : towers) {
			if (tower.distanceSquaredTo(next) <= RobotType.TOWER.attackRadiusSquared)
				return false;
		}
		if (hq.distanceSquaredTo(next) <= HQrange)
			return false;
		return true;
	}

	protected void moveSafeAll() throws GameActionException {
		if (rand.nextFloat() > 0.9) {
			if (rand.nextFloat() > 0.5)
				facing = facing.rotateLeft();
			else
				facing = facing.rotateRight();
		}
		for (RobotInfo enemy : enemies) {
			if (enemy.type == RobotType.MISSILE
					&& enemy.location.distanceSquaredTo(rc.getLocation()) < 10)
				facing = enemy.location.directionTo(rc.getLocation());
		}
		outer: for (int p = 0; p++ < 8; facing = left ? facing.rotateLeft()
				: facing.rotateRight()) {
			if (!rc.canMove(facing))
				continue;
			MapLocation next = rc.getLocation().add(facing);
			for (RobotInfo enemy : enemies) {
				if (enemy.location.distanceSquaredTo(next) <= enemy.type.attackRadiusSquared)
					continue outer;
			}
			for (MapLocation tower : towers) {
				if (tower.distanceSquaredTo(next) <= RobotType.TOWER.attackRadiusSquared)
					continue outer;
			}
			if (hq.distanceSquaredTo(next) <= HQrange)
				continue outer;
			rc.move(facing);
			break;
		}
	}

	static int HQrange = 54;

	protected void moveSafe() throws GameActionException {
		if (rand.nextFloat() > 0.9) {
			if (rand.nextFloat() > 0.5)
				facing = facing.rotateLeft();
			else
				facing = facing.rotateRight();
		}
		for (RobotInfo enemy : enemies) {
			if (enemy.type == RobotType.MISSILE
					&& enemy.location.distanceSquaredTo(rc.getLocation()) < 10)
				facing = enemy.location.directionTo(rc.getLocation());
		}
		outer: for (int p = 0; p++ < 8; facing = left ? facing.rotateLeft()
				: facing.rotateRight()) {
			if (!rc.canMove(facing))
				continue;
			MapLocation next = rc.getLocation().add(facing);
			for (MapLocation tower : towers) {
				if (tower.distanceSquaredTo(next) <= RobotType.TOWER.attackRadiusSquared)
					continue outer;
			}
			if (hq.distanceSquaredTo(next) <= HQrange)
				continue outer;
			rc.move(facing);
			break;
		}
	}

	protected void moveSomewhere() throws GameActionException {
		if (rand.nextFloat() > 0.9) {
			if (rand.nextFloat() > 0.5)
				facing = facing.rotateLeft();
			else
				facing = facing.rotateRight();
		}
		for (RobotInfo enemy : enemies) {
			if (enemy.type == RobotType.MISSILE
					&& enemy.location.distanceSquaredTo(rc.getLocation()) < 10)
				facing = enemy.location.directionTo(rc.getLocation());
		}
		outer: for (int p = 0; p++ < 8; facing = left ? facing.rotateLeft()
				: facing.rotateRight()) {
			if (!rc.canMove(facing))
				continue;
			MapLocation next = rc.getLocation().add(facing);
			if (towers.length > 2 && hq.distanceSquaredTo(next) <= HQrange)
				continue outer;
			rc.move(facing);
			break;
		}
	}

	protected void move() throws GameActionException {
		if (rand.nextFloat() > 0.9) {
			if (rand.nextFloat() > 0.5)
				facing = facing.rotateLeft();
			else
				facing = facing.rotateRight();
		}
		outer: for (int p = 0; p++ < 8; facing = left ? facing.rotateLeft()
				: facing.rotateRight()) {
			if (!rc.canMove(facing))
				continue;
			rc.move(facing);
			break;
		}
	}

	int k = -1;

	protected void mineMove() throws GameActionException {
		do {
			if (k == 0 || !isSafe(rc.getLocation())) {
				survey();
				moveSafeAll();
			} else {
				if (k < 0) {
					k = (int) (rc.senseOre(rc.getLocation()) / 3 + 0.99);
					if (k == 0)
						continue;
					if (k > 6)
						k = 6;
				}
				rc.mine();
			}
			k--;
		} while (false);
	}

	void survey() throws GameActionException {
		MapLocation res = rc.getLocation();
		double ore = rc.senseOre(res);
		Direction d = facing;
		for (int i = 0; i < 8; i++, d = d.rotateRight()) {
			double tmp = rc.senseOre(res.add(d));
			if (tmp > ore) {
				if (rc.senseNearbyRobots(res.add(d), 0, myTeam).length == 0) {
					facing = d;
					ore = tmp;
				}
			}
		}
	}

	int surveyTot() {
		int tot = 0;
		for (Direction d : directions) {
			tot += rc.senseOre(myLoc.add(d));
		}
		return tot;
	}
}
