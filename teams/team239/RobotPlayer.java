package team239;

import battlecode.common.*;

import java.util.*;

public class RobotPlayer {

	public static void run(RobotController rc) {
		Robot rob;
		switch (rc.getType()) {
		case HQ:
			rob = new HQ(rc);
			break;
		case BEAVER:
			rob = new Beaver(rc);
			break;
		case TOWER:
			rob = new Building(rc, null);
			break;
		case MINERFACTORY:
			try {
				rc.broadcast(1, 0);
			} catch (GameActionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			rob = new MinerFactory(rc);
			break;
		case BARRACKS:
			rob = new Building(rc, null);
			break;
		case HELIPAD:
			rob = new Helipad(rc);
			break;
		case TANKFACTORY:
			rob = new Building(rc, RobotType.TANK);
			break;
		case AEROSPACELAB:
			rob = new Building(rc, RobotType.LAUNCHER);
			break;
		case TRAININGFIELD:
			rob = new Building(rc, RobotType.COMMANDER);
			break;
		case MINER:
			rob = new Miner(rc);
			break;
		case DRONE:
			rob = new Drone(rc);
			break;
		case MISSILE:
			rob = new Missile(rc);
			break;
		default:
			if (!rc.getType().isBuilding) {
				rob = new Unit(rc);
			} else {
				for (;;)
					rc.yield();
			}
		}
		rob.start();
	}
}
