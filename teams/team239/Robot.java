package team239;

import java.util.Random;

import battlecode.common.*;

public abstract class Robot {
	int sparse = 0;
	protected RobotController rc;
	Team myTeam;
	protected Team enemyTeam;
	int myRange, order, globalOrder, HQid, birthTime;
	Random rand;
	protected float chance;
	boolean hasWeapon, canMove;
	protected boolean left;
	protected Direction facing;
	MapLocation myHq, myLoc;
	Direction[] directions = { Direction.NORTH, Direction.NORTH_EAST,
			Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH,
			Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST };

	Robot(RobotController rcc) {
		rc = rcc;
		myLoc = rc.getLocation();
		myHq = rc.senseHQLocation();
		birthTime = Clock.getRoundNum();
		myTeam = rc.getTeam();
		enemyTeam = myTeam.opponent();
		rc = rcc;
		myRange = rc.getType().attackRadiusSquared;
		rand = new Random(rc.getID());
		left = rand.nextBoolean();
		facing = directions[rand.nextInt(8)];
	}

	public void start() {
		while (true) {
			rc.yield();
		}
	}

	protected boolean spawn(RobotType t) throws GameActionException {
		Direction f = directions[rand.nextInt(8)];
		for (int i = 0; i < 8; i++) {
			if (rc.canSpawn(f, t)) {
				rc.spawn(f, t);
				return true;
			}
			f = f.rotateLeft();
		}
		return false;
	}

	void initializeTurn() throws GameActionException {
		chance = rand.nextFloat();
		globalOrder = rc.readBroadcast(0);
	}

	protected void attackSomething() throws GameActionException {
		RobotInfo r[] = rc.senseNearbyRobots(myRange, enemyTeam);
		MapLocation target = null;
		double hp = 1000000;
		for (RobotInfo ri : r) {
			double temp = ri.health;
			if (ri.type == RobotType.MISSILE)
				temp *= 2000;
			if (ri.type == RobotType.TOWER)
				temp = 8;
			if (ri.type == RobotType.HQ)
				temp = -1;
			if (temp < hp) {
				hp = temp;
				target = ri.location;
			}
		}
		if (target != null)
			rc.attackLocation(target);
	}

	protected void supply() throws GameActionException {
		RobotInfo nearbyAllies[] = rc.senseNearbyRobots(rc.getLocation(),
				GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, myTeam);
		double lowestSupply = rc.getSupplyLevel();
		if (lowestSupply < 20)
			return;
		MapLocation supplyTarget = null;
		for (RobotInfo ally : nearbyAllies) {
			if (needSupply(ally.type) && ally.supplyLevel < lowestSupply) {
				lowestSupply = ally.supplyLevel;
				supplyTarget = ally.location;
			}
		}
		if (supplyTarget != null) {
			rc.transferSupplies((int) (rc.getSupplyLevel() - lowestSupply) / 2,
					supplyTarget);
		}
	}

	boolean needSupply(RobotType r) {
		switch (r) {
		case BEAVER:
		case SOLDIER:
		case MINER:
		case COMPUTER:
		case BASHER:
		case DRONE:
		case TANK:
		case COMMANDER:
		case LAUNCHER:
			return true;
		default:
			return false;
		}
	}

	int locationId(MapLocation l) {
		return 100 + l.x + 130 * l.y;
	}

	MapLocation idLocation(int id) {
		id -= 100;
		return new MapLocation(id % 130, id / 130);
	}

	MapLocation hqSplashAttack(MapLocation hq, MapLocation target) {
		int x = target.x;
		int y = target.y;
		if (x < hq.x)
			x++;
		else if (x > hq.x)
			x--;
		if (y < hq.y)
			y++;
		else if (y > hq.y)
			y--;
		MapLocation t = new MapLocation(x, y);
		if (t.distanceSquaredTo(hq) <= GameConstants.HQ_BUFFED_ATTACK_RADIUS_SQUARED)
			return t;
		return null;
	}

}
