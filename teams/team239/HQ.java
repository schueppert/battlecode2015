package team239;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class HQ extends Building {

	HQ(RobotController rc) {
		super(rc, RobotType.BEAVER);

	}

	int attack = 1800;

	@Override
	public void start() {

		try {
			spawn(buildOrder);
			rc.yield();
		} catch (GameActionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (true) {
			try {
				initializeTurn();
				supply();
				if (Clock.getRoundNum() == attack)
					rc.broadcast(0, 2);
				if (Clock.getRoundNum() == attack - 10)
					rc.broadcast(0, 1);
				else if (Clock.getRoundNum() == 200)
					rc.broadcast(2, 1);
				if (rc.isWeaponReady()) {
					int k = rc.senseTowerLocations().length;
					if (k >= 5) {
						myRange = 35;
						RobotInfo[] enemies = rc.senseNearbyRobots(52,
								enemyTeam);
						for (RobotInfo enemy : enemies) {
							MapLocation t = hqSplashAttack(rc.getLocation(),
									enemy.location);
							if (t != null) {
								rc.attackLocation(t);
								break;
							}
						}
					} else if (k >= 2) {
						myRange = 35;
						attackSomething();
					} else {
						myRange = 24;
						attackSomething();
					}
				}
				if ((rc.getTeamOre() > 600) && rc.isCoreReady()
						&& buildOrder != null && Clock.getRoundNum() % 25 == 0) {
					spawn(buildOrder);
				}
				rc.yield();
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}
}
