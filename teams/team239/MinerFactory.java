package team239;

import battlecode.common.*;

public class MinerFactory extends Building {
	RobotType buildOrder;

	MinerFactory(RobotController rc) {
		super(rc, RobotType.MINER);
	}

	@Override
	public void start() {
		while (true) {
			try {
				int bopt = rc.readBroadcast(1);
				initializeTurn();
				if (rc.isWeaponReady()) {
					attackSomething();
				}
				if (rc.isCoreReady() && Clock.getRoundNum() < 1400) {
					spawn(RobotType.MINER);
				}
				rc.yield();
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}

}
