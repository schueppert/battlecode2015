package team239;

import battlecode.common.*;

public class Beaver extends Unit {
	int buildConst;
	boolean first = false;
	MapLocation myHQ;

	Beaver(RobotController rcc) {
		super(rcc);
		buildConst = (rc.senseHQLocation().x + rc.senseHQLocation().y) % 2;
		myHQ = rc.senseHQLocation();
		if (birthTime == 0) {
			firstBeaverOrder();
			first = true;
		}
	}

	RobotType spam = null;

	@Override
	public void defaultStrat() throws GameActionException {
		sparse = rc.readBroadcast(1);
		if (sparse == -1) {
			if (surveyTot() >= 40) {
				rc.broadcast(1, 1);
				sparse = 1;
			}
		}
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		if (rc.isCoreReady()) {
			if (sparse != -1) {
				facing = rc.getLocation().directionTo(myHq);
			}
			if (Clock.getRoundNum() > 50
					&& rc.senseRobotAtLocation(rc.senseHQLocation()).supplyLevel < 2500 + Clock
							.getRoundNum())
				spam = RobotType.SUPPLYDEPOT;
			else if (sparse == 1)
				spam = RobotType.MINERFACTORY;
			else if (Clock.getRoundNum() > 200)
				spam = RobotType.HELIPAD;
			else
				spam = null;
			if (spam != null && rc.getTeamOre() > spam.oreCost && build(spam)) {
				if (spam == RobotType.MINERFACTORY)
					rc.broadcast(1, 0);
			} else
				mineMove();
		}
	}

	private void firstBeaverOrder() {
		RobotType bo[] = { RobotType.MINERFACTORY, RobotType.HELIPAD,
				RobotType.HELIPAD };
		if (40 > surveyTot()) {
			try {
				sparse = -1;
				bo = new RobotType[] { RobotType.HELIPAD };
				rc.broadcast(1, -1);
			} catch (GameActionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		spam = RobotType.MINERFACTORY;
		int k = 0;
		stay: while (true) {
			if (rc.getTeamOre() > 550 || Clock.getRoundNum() > 200)
				break;
			try {
				sparse = rc.readBroadcast(1);
			} catch (Exception e) {

			}
			facing = rc.getLocation().directionTo(myHQ);
			if (rc.getCoreDelay() < 1) {
				if (rc.getTeamOre() >= spam.oreCost) {
					try {
						k += startBuild(bo[k]) ? 1 : 0;
						if (k >= bo.length)
							break stay;
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
				if (rc.getCoreDelay() < 1) {
					try {
						mineMove();
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
			}
			rc.yield();
		}
		spam = null;
	}

	protected boolean startBuild(RobotType t) throws GameActionException {
		MapLocation l = rc.getLocation();
		Direction f = l.directionTo(myHQ);
		f.rotateRight();
		for (int i = 0; i < 8; i++, f = f.rotateLeft()) {
			MapLocation temp = l.add(f);
			if (temp.distanceSquaredTo(myHQ) > 2)
				continue;
			if (rc.canBuild(f, t)) {
				rc.build(f, t);
				return true;
			}
		}
		return false;
	}

	protected boolean build(RobotType t) throws GameActionException {
		MapLocation l = rc.getLocation();
		Direction f = l.directionTo(rc.senseHQLocation());
		f.rotateRight();
		for (int i = 0; i < 8; i++, f = f.rotateLeft()) {
			MapLocation temp = l.add(f);
			if ((temp.x + temp.y) % 2 == buildConst)
				continue;
			if (rc.canBuild(f, t)) {
				rc.build(f, t);
				return true;
			}
		}
		return false;
	}

}
