package team239;

import battlecode.common.*;

public class Building extends Robot {
	protected RobotType buildOrder;

	public Building(RobotController rc, RobotType buildOrder) {
		super(rc);
		this.buildOrder = buildOrder;
	}

	@Override
	public void start() {
		while (true) {
			try {
				initializeTurn();
				if (rc.isWeaponReady()) {
					attackSomething();
				}
				if (rc.isCoreReady() && buildOrder != null) {
					spawn(buildOrder);
				}
				rc.yield();
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}

}
