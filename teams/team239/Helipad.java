package team239;

import battlecode.common.*;

public class Helipad extends Building {
	RobotType buildOrder;

	Helipad(RobotController rc) {
		super(rc, RobotType.DRONE);
	}

	@Override
	public void start() {
		while (true) {
			try {
				int bopt = rc.readBroadcast(1);
				initializeTurn();
				if (rc.isWeaponReady()) {
					attackSomething();
				}
				if (rc.isCoreReady()) {
					spawn(RobotType.DRONE);
				}
				rc.yield();
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}

}
